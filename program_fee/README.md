# Program Fee Message Payload Definition
This message handles Program Fee for the enterprise. Each message should contain data for a single Program Fee.

Required Fields
* client_id
* client_id_legacy
* program_fee
* program_fee_id
* program_id

JSON Name | Comment
--- | ---
card_type | An English description for the card type.
client_id | The owner system identifier for the client program for this transaction
client_id_legacy | The two character client (network) identifier
credit_debit | If the program fee is for credits or debits.
effective_ts | Date/Time that the program fee is effective
network_active | Is the network active Y/N
network_name | Business Unit Network Name.
network_service_type | Service Type mapped to the network and processing style.
program_fee | The fee percentage to be charged.
program_fee_id | Id of the program fee
program_ id| The program under the client Apruve, e.g., Apruve. Once assigned it cannot change.
qualification | Defines the transaction qualification the fee is for. The qualification is assigned at the transaction level during transaction processing.
risk_level | Defines the risk level that the fee is for. The risk level is assigned at the detail level during transaction processing.