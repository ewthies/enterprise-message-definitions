require 'json_schemer'
require 'net/http'

RSpec.describe 'keying/schema.json' do
  let(:schema_path) { Pathname.new('keying/schema.json') }

  context 'using json-schema draft-07' do
    # Fortinet is blocking access to the meta schema, so we must use a local copy instead.
    # let(:schema) { JSONSchemer.schema(Net::HTTP.get(URI.parse('http://json-schema.org/draft-07/schema'))) }
    let(:meta_schema_path) { Pathname.new('spec/files/json-schema.draft-07.schema.json') }
    let (:schema) { JSONSchemer.schema(meta_schema_path) }
    subject { JSON.parse(schema_path.read) }

    it 'validates against the meta-schema' do
      expect( schema.validate(subject).to_a ).to eq([])
    end
  end

  describe 'sample.json' do
    let(:schema) { JSONSchemer.schema(schema_path) }
    let(:sample_path) { Pathname.new('keying/sample.json') }
    subject {JSON.parse(sample_path.read)}

    it 'validates' do
      expect( schema.validate(subject).to_a ).to eq([])
    end
  end

  describe 'gitlab/architecture/enterprise-message-definitions/raw/master/keying/sample.json' do
    let(:schema) { JSONSchemer.schema(schema_path) }

    response = Net::HTTP.get(URI('https://scm.multiservice.com/gitlab/architecture/enterprise-message-definitions/raw/master/keying/sample.json'))

    # If it finds the schema (gets a 200 response) then validate else skip the test.
    if response.is_a?(Net::HTTPSuccess)
      subject {JSON.parse(response)}
      it 'validates against the schema in master branch' do
        expect( schema.validate(subject).to_a ).to eq([])
      end
    else
      it "is skipped (brand new sample file)" do
        skip
      end
    end
  end

end
