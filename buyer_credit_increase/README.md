# Credit Increase Message Payload Definition
This message handles Credit Increase for the enterprise.

Required Fields
* buyer_id
* client_id
* client_id_legacy
* program_id 

JSON Name | Comment
--- | ---
account\_opened | The date account was opened.
amount\_requested | The amount requested by the buyer.
begin\_ts | First comment timestamp.
begin\_user | User who entered first comment on the credit increase application
buyer\_id | Buyer ID assigned by the system. Column Name: Customer ID
buyer\_name | "The full, legal name of the buyer."
client\_id | For GUESS projects this is analogous to the Network.
client\_id\_legacy | For ALL projects this will be the 2-letter Network Identifier.
company\_type | The buyer company type. Column Name: Company Type.
elapsed\_days.elapsed\_time\_business\_days | Credit Increase application business days duration.
elapsed\_days.elapsed\_time\_days | Credit Increase application duration in days. Column Name: Elapsed days
elapsed\_minutes.elapsed\_time\_business\_minutes | Credit Increase application business days duration in minutes.
elapsed\_minutes.elapsed\_time\_minutes | Credit Increase application duration in minutes. Column Name: Elapsed Time Minutes
end\_ts | Last comment timestamp
end\_user | User who entered last comment on the credit increase application
entry\_origin | The request origin. eg. online application, email, fax or phone
entry\_source | Application request entry source. eg. website or system.
program\_id | The program under the client Apruve, e.g., Apruve.
request\_id | The application request id.
request\_state | The state of the application. eg. Open or closed
request\_status | Status of the credit line increase application.
request\_type | The type of request.
request\_type\_text | Type of request in text.


