# Buyer Message Payload Definition

JSON Name | Comment
--- | ---
active\_cards | Number of active cards that are set up for this customer. Column Name: Active Cards
additional\_address\_avs | Additional contact type address line 1 with only numeric characters used for AVS matching.
additional\_phone | Additional contact type comma separated list of phone numbers not including fax numbers
alternate\_client\_identifier | A second client identifier used by some projects.
application\_submitted\_at | Date the buyer submitted their application to join the program
ar\_owner | Accounts Receivable Owner - Who's bank account are payments going through for this transaction and who is responsible for exposure if not in recourse. Column Name: AR Owner
available\_credit\_type | Defines if the customer has their own established credit or is defined by a credit customer with full or limited access. Column Name: Available Credit Type
bill\_cycle | Customers billing cycle. Defines how often a customer is billed. Column Name: Billing Cycle
bill\_parent | Do we bill this customer (N) or the customer's parent (Y)? Column Name: Bill Parent
billing\_contact.address\_avs | Line 1 of customer billing address with only numeric charaters used for AVS matching.
billing\_contact.city | Customer billing address city. Column Name: City
billing\_contact.contact | Customer billing contact. This will be who to contact in regards to billing matters. Column Name: Contact
billing\_contact.country | Customers billing address country. Column Name: Country
billing\_contact.email\_address | Customer billing contact email address. Column Name: Email Address
billing\_contact.phone | Customer billing contact phone number. Column Name: Phone Number
billing\_contact.post\_code | Customer billing address postal code. Column Name: Postal Code
billing\_contact.state | Customer billing address state. Column Name: State
billing\_contact.street | Customer billing address street. This can be made up of multiple address lines. Column Name: Street
buyer\_id | Customer ID assigned by the system. Column Name: ID
charge\_late\_fees | Is the customer set to be charged late fees Y(es) or N(o)? Column Name: Charge Late Fees? (Y/N)
client\_division | Client-privided business division.
client\_id | The GUID for the client, e.g., Apruve is the client and it's the GUID for Apruve. 
client\_id\_legacy | Customers network identifier. Column Name: Network ID
client\_identifier | The clients identifier for the customer. If blank then the project either does not have a client or the client did not provide their identifier for this customer. Column Name: Client Identifier
company\_type | The customers company type. Column Name: Company Type
country | Customers country. Column Name: Account Country
created\_at | Date the customer was initially created in the system. Column Name: Create Date
credit\_type | The types of measures used to determine customer credit line. e.g. Letter of Credit, Dun and Bradstreet rating. Column Name: Credit Type
credit\_rep | The credit representative assigned to the customer.
credit\_rep\_assign\_date | The date the credit representative was assigned to the customer.
credit.buyer\_id | The credit customers ID. This will be the same as the customer ID if getting their own credit. Column Name: Credit Customer ID
credit.buyer\_name | The credit customers legal name. This will be the same as the customer name if getting their own credit. Column Name: Credit Customer Name
credit.hard\_credit | Is the credit customer set to have hard credit Y(es) or N(o)? Hard credit Y means that we will decline authorizations and transactions if they are over their credit line. Column Name: Hard Credit Limit? (Y/N)
credit.rep | The credit representative assigned to the credit customer. Column Name: Credit Rep
credit.rep\_assign\_date | The date the credit representative was assigned to the credit customer. Column Name: Credit Rep Assign Date
credit.tax\_id | The credit customers tax identifier. Column Name: Credit Tax ID
credit.tier | The credit customers tier. This is used to identify (white glove) customers. Column Name: Credit Tier
currencies.buyer | Customer currency. Column Name: Currency
currencies.secondary | Client Currency - 3 Character currency code e.g. USD. Column Name: Client Currency
currencies.system | System Currency - 3 Character currency code e.g. USD. Column Name: System Currency
current\_days\_to\_pay | Customers current days to pay. This is calculated by taking the last four applied payments and averaging the number of days between the billing date and payment date. Column Name: Current Days to Pay
dba\_name | Customer Doing Business As name. Not the legal name.
direct\_debit | Is the customer set to up for us to direct debit them when bills become due Y(es) or N(o)? Column Name: Direct Debit? (Y/N)
duns\_number | Customer Dun and Bradstreet number.
edi\_integration | Is the customer set up to get transactions via EDI Y(es) or N(o)? If blank then the project does not support this functionality. Column Name: EDI Integration? (Y/N)
electronic\_billing | Is the customer set up to get bills only by electronic delivery methods Y(es) or N(o)? Column Name: Electronic Billing? (Y/N)
first\_activation\_date | Date the customer was initially activated in the system. This is the date they would first be eligible to purchase. Column Name: Initial Activation Date
first\_deposit\_date | Date the customer first submitted a payment.
first\_email\_open\_date | The first date that the customer opened their invoice email.
first\_purchase\_date | Customers first date they made a purchase. Determined by transaction date. Column Name: First Transaction Date
in\_recourse | Is the customer in recourse Y(es) or N(o)? Recourse means that the responsibility of the exposure of the accounts receivable for the customer has been accepted by the client or merchant. Column Name: In Recourse? (Y/N)
include\_children\_invoices | Should the invoice copy include children transactions? Column Name: Include Children Invocies
included\_invoice\_option | The included invoice option for my transactions. Column Name: Included Invoice Option
is\_included\_inv\_copy | Does the bill statement include invoice copies? Column Name: Include Invoice Copies
language | Customer language. Column Name: Language
last\_comment.author | User who entered last comment for customer. Column Name: Last Comment User
last\_comment.date | Date the last user comment was entered for customer. Column Name: Last Comment Date
last\_comment.text | Last user comment entered for customer. Column Name: Last Comment
last\_deactive\_date | Customers last date they were deactivated in the system. Column Name: Deactivation Date
last\_deactive\_reason | Reason the customer was last deactivated. Column Name: Deactivation Reason
last\_deposit\_date | Last deposit date that a payment was posted to the customers account. Column Name: Last Deposit Date
last\_purchase\_date | Customers last date they made a purchase. Determined by transaction date. Column Name: Last Purchase Date
late\_grace\_days | Number of days past the due date that the system will allow before charging late charges on an invoice. Column Name: Grace Period
legal\_signatory.legal\_signatory\_full\_name | Full legal name of signatory.
legal\_signatory.legal\_signatory\_dob | Legal signatory date of birth.
lytd\_days\_to\_pay | Last year to date days to pay. This is calculated by the average number of days between the billing date and payment date for all payments deposited from first day of last year to todays date last year.  Column Name: Days to Pay LYTD
name | The full, legal name of the customer. Column Name: Name
network\_name | Business Unit Network Name, for the project eViva, this would be Cummins. 
parent\_buyer\_id | Parent customer ID for this customer. If blank then customer does not have a parent. Column Name: Parent Customer ID
payment\_due\_days | Customers payment terms. The due date for an invoice is determined by billing date + customers payment terms. Column Name: Payment Terms
program\_id | The program under the client Apruve, e.g., Apruve or Cummins. Once assigned it cannot change.
report\_bill | If this customer is not being billed directly (Bill\_Parent = 'Y'), does the customer still receive a copy of the bill? Column Name: Report Bill
service\_fee\_buyer | Is the customer used as a customer to bill merchant service fees to Y(es) or N(o)? If blank then the project does not support this functionality. Column Name: Service Fee Customer? (Y/N)
status | Customers currenty account status. Column Name: Status
time\_zone | Customers time zone. If blank then the project does not support time zone.  Column Name: Time Zone
updated\_at | The most recent datetime data for this buyer was updated in the origin system
w9.tax\_account\_type | 
w9.tax\_classification | 
w9.tax\_exempt | 
w9.tax\_id | Comma separated list of customers tax id's.
ytd\_days\_to\_pay | This year to date days to pay. This is calculated by the average number of days between the billing date and payment date for all payments deposited from first day of this year to todays date. Column Name: Days To Pay YTD

## Notes
