# Buyer Application Message Payload Definition
This message handles Buyer Application for the enterprise. Each message should contain data for a single application.

Required Fields
* application_id
* client_id
* client_id_legacy
* program_id 

JSON Name | Comment
--- | ---
amount\_credit | Credit line assigned to the billing buyer. If they are drawing their credit of their parent then this will define the billing buyer
application\_id | This will be the application ID. Usually the primary key
application\_source | The source where the application came in from.
application\_status | The status of the application e.g. Pending Review, Potential-Duplicate. Column Name: Status
auto\_approved | Shows application was automatically approved or not.
buyer\_id | Buyer ID assigned by the system. Column Name: Customer ID
buyer\_name | The buyer name. Column Name: Company name
client\_id | For GUESS projects this is analogous to the Network.
client\_id\_legacy | For ALL projects this will be the 2-letter Network Identifier.
elapsed\_time.elapsed\_time\_business\_minutes | Application business days duration in minutes
elapsed\_time.elapsed\_time\_minutes | Application duration in minutes. Column Name: Elapsed Time Minutes
program\_id | The program under the client Apruve, e.g., Apruve.
requested\_credit\_limit | The buyer requested credit limit
start\_ts | Application start time.
stop\_ts | Application stop time.
summary\_status | The application summary status. Column Name: Summary Status

