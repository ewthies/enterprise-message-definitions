# Dispute Message Payload Definition

This message handles disputes for the enterprise. Each message should contain data for a single dispute.

Required Fields
* client_id
* client_id_legacy
* dispute_id
* program_id
* transaction_id

JSON Name | Comment
--- | ---
ar_buyer_id | Max billable customer ID. Column Name: Max Billing Customer ID
bill_date | Billing date for transaction. This is the date used to determine the due date based on customers payment terms. Column Name: Bill Date
business_days_in_dispute | The amount of business days from dispute date to current date or resolution date if resolved. M-F 8:00AM-5:00PM No Holidays. Column Name: Business Days in Dispute
business_minutes_in_dispute | The amount of business minutes from dispute date to current date or resolution date if resolved. M-F 8:00AM-5:00PM No Holidays. Column Name: Business Minutes in Dispute
business_minutes_to_dispute | The amount of business minutes from bill date to dispute date. M-F 8:00AM-5:00PM No Holidays. Column Name: Business Minutes to Dispute
client_id | For GUESS projects this is analogous to the Network. So for 'PACCAR - Mexico' they will submit 'PM'. The network/client Cummins would submit 'CU'. For Australia projects, this will be the ID for the client, e.g., 74243728-8819-4cc0-9585-774e2ab1e7fc as the ID for Apruve. This value cannot change and must be unique.
client_id_legacy | For ALL projects this will be the 2-letter Network Identifier. So 'PACCAR - Australia' would submit 'PA', PACCAR would submit 'PP', Cummins would submit 'CU'.
currencies.buyer | Customer Currency - 3 Character currency code e.g. USD. Column Name: Customer Currency
currencies.secondary | Client Currency - 3 Character currency code e.g. USD. Column Name: Client Currency
currencies.system | System Currency - 3 Character currency code e.g. USD. Column Name: System Currency
days_in_dispute | The amount of time in days from dispute date to current date or resolution date if resolved. Column Name: Days in Dispute
dispute_date | The date/time dispute was initiated. Column Name: Dispute Date
dispute_id | Unique identifier for a dispute event. Column Name: Dispute ID
dispute_reason | Reason the dispute was initiated. Column Name: Dispute Reason
dispute_status | Current status of dispute. Column Name: Dispute Status
dispute_user | User who initiated the dispute. Column Name: Dispute User
disputed_total.buyer_amount | Disputed total in customer currency. Column Name: Disputed Total (Customer Currency)
disputed_total.secondary_amount | Disputed total in client currency. Column Name: Disputed Total (Client Currency)
disputed_total.system_amount | Disputed total in system currency. Column Name: Disputed Total (System Currency)
first_dispute_comment | First comment entered about dispute. Column Name: First Dispute Comment
invoice_date | The date/time for this transaction. This will normally be in the time zone of the merchant unless it is an internally generated transaction, those will be in system timezone. Column Name: Invoice Date
last_dispute_comment | Last comment entered about dispute. Column Name: Last Dispute Comment
last_dispute_comment_date | The date/time the last dispute comment was entered. Column Name: Last Dispute Comment Date
last_dispute_comment_user | User who entered last dispute comment. Column Name: Last Dispute Comment User
minutes_in_dispute | The amount of time in minutes from dispute date to current date or resolution date if resolved. Column Name: Minutes in Dispute
minutes_to_dispute | The amount of time in minutes from bill date to dispute date. Column Name: Minutes to Dispute
network_name | Business Unit Network Name. Column Name: Network Name
po_number | Purchase Order Number, this is used by some systems to enforce PO purchase policies. Column Name: PO Number
processing_date | Processing date. This will be the day this processing run is for. Column Name: Processing Date
program_id | The program under the client Apruve, e.g., Apruve.
purchasing_buyer_credit_rep | Customer credit representative. Column Name: Purchasing Customer Credit Rep
purchasing_buyer_id | Customer that the card belongs to at time of transaction based on transaction date or the customer the transaction belongs to if the transaction is not card based.  If the transaction is a Bank Card purchasing card, the associated customer will populate this column. This can be null if it is a third party card where we do not know the customer. Column Name: Purchasing Customer ID
purchasing_buyer_name | The full, legal name of the customer. Column Name: Purchasing Customer Name
purchasing_seller_client_id | Client identifier for purchasing merchant. Column Name: Client Merchant ID
purchasing_seller_id | Filled in with the merchant business unit that delivered the batch of transactions. This could be assigned based on the cm_id from the batch or assigned based on the third party processor that sent us the batch/transactions.  Not all transactions (e.g. internally generated) will be associated with a Batch. Column Name: Transacting Merchant ID
purchasing_seller_name | The full, legal name of the Merchant. Column Name: Transacting Merchant Name
request_id | The request management system ID this dispute is related to. Column Name: Request ID
resolution_date | The date/time the dispute was resolved. Column Name: Resolution Date
transaction_id | Transaction ID. Column Name: Transaction ID
transaction_reference | Transaction reference assigned by merchant and usually the one printed on the customer's ticket. Column Name: Invoice Number

## Notes
