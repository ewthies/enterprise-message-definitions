# Accounts Payable Message Payload Definition

This message handles Seller Accounts Payable for the enterprise.
Each message should contain accouts payable information for a seller if payment groups are not supported or for a seller/payment group if payment groups are supported.
The payable_lt_1_day_total + payable_1_day_total through payable_14_day_total + payable_gte_15_day_total should equal the pending_total in all currencies.

Required Fields
* client_id
* client_id_legacy
* currencies
    * seller
    * system
* program_id
* seller_id

JSON Name | Comment
--- | ---
ap_status | Current status of Accounts Payable for Seller e.g. Pending to be paid, Held - Administrative Hold, and Held - Negative Amount.
bank_account_last_4 | Last 4 digits of current bank account for Seller.
banking_country | Banking country of Seller. This is sometimes different than the physical or remittance location of the Seller.
client_id | For GUESS projects this is analogous to the Network. So for 'PACCAR - Mexico' they will submit 'PM'. The network/client Cummins would submit 'CU'. For Australia projects, this will be the ID for the client, e.g., 74243728-8819-4cc0-9585-774e2ab1e7fc as the ID for Apruve. This value cannot change and must be unique.
client_id_legacy | For ALL projects this will be the 2-letter Network Identifier. So 'PACCAR - Australia' would submit 'PA', PACCAR would submit 'PP', Cummins would submit 'CU'.
client_identifier | The clients identifier for the customer. If blank then the project either does not have a client or the client did not provide their identifier for this customer. Column Name: Client Identifier
currencies.seller | Currency in which we are paying the Seller 
currencies.system | System currency of originating program.
held_total.seller_amount | Total amount of payable that is held in seller currency.
held_total.system_amount | Total amount of payable that is held in system currency.
in_transit_today_total.seller_amount | Paid amount that is in transit and expected to settle today in seller currency.
in_transit_today_total.system_amount | Paid amount that is in transit and expected to settle today in system currency.
in_transit_1_day_total.seller_amount | Paid amount that is in transit and expected to settle in 1 day in seller currency.
in_transit_1_day_total.system_amount | Paid amount that is in transit and expected to settle in 1 day in system currency.
in_transit_2_day_total.seller_amount | Paid amount that is in transit and expected to settle in 2 days in seller currency.
in_transit_2_day_total.system_amount | Paid amount that is in transit and expected to settle in 2 days in system currency.
in_transit_gte_3_day_total.seller_amount | Paid amount that is in transit and expected to settle in greater than or equal to 3 days in seller currency.
in_transit_gte_3_day_total.system_amount | Paid amount that is in transit and expected to settle in greater than or equal to 3 days in system currency.
payable_lt_1_day_total.seller_amount | Payable non-held amount that is expected to settle in less than 1 day in seller currency.
payable_lt_1_day_total.system_amount | Payable non-held amount that is expected to settle in less than 1 day in system currency.
payable_1_day_total.seller_amount | Payable non-held amount that is expected to settle in 1 day in seller currency.
payable_1_day_total.system_amount | Payable non-held amount that is expected to settle in 1 day in system currency.
payable_2_day_total.seller_amount | Payable non-held amount that is expected to settle in 2 days in seller currency.
payable_2_day_total.system_amount | Payable non-held amount that is expected to settle in 2 days in system currency.
payable_3_day_total.seller_amount | Payable non-held amount that is expected to settle in 3 days in seller currency.
payable_3_day_total.system_amount | Payable non-held amount that is expected to settle in 3 days in system currency.
payable_4_day_total.seller_amount | Payable non-held amount that is expected to settle in 4 days in seller currency.
payable_4_day_total.system_amount | Payable non-held amount that is expected to settle in 4 days in system currency.
payable_5_day_total.seller_amount | Payable non-held amount that is expected to settle in 5 days in seller currency.
payable_5_day_total.system_amount | Payable non-held amount that is expected to settle in 5 days in system currency.
payable_6_day_total.seller_amount | Payable non-held amount that is expected to settle in 6 days in seller currency.
payable_6_day_total.system_amount | Payable non-held amount that is expected to settle in 6 days in system currency.
payable_7_day_total.seller_amount | Payable non-held amount that is expected to settle in 7 days in seller currency.
payable_7_day_total.system_amount | Payable non-held amount that is expected to settle in 7 days in system currency.
payable_8_day_total.seller_amount | Payable non-held amount that is expected to settle in 8 days in seller currency.
payable_8_day_total.system_amount | Payable non-held amount that is expected to settle in 8 days in system currency.
payable_9_day_total.seller_amount | Payable non-held amount that is expected to settle in 9 days in seller currency.
payable_9_day_total.system_amount | Payable non-held amount that is expected to settle in 9 days in system currency.
payable_10_day_total.seller_amount | Payable non-held amount that is expected to settle in 10 days in seller currency.
payable_10_day_total.system_amount | Payable non-held amount that is expected to settle in 10 days in system currency.
payable_11_day_total.seller_amount | Payable non-held amount that is expected to settle in 11 days in seller currency.
payable_11_day_total.system_amount | Payable non-held amount that is expected to settle in 11 days in system currency.
payable_12_day_total.seller_amount | Payable non-held amount that is expected to settle in 12 days in seller currency.
payable_12_day_total.system_amount | Payable non-held amount that is expected to settle in 12 days in system currency.
payable_13_day_total.seller_amount | Payable non-held amount that is expected to settle in 13 days in seller currency.
payable_13_day_total.system_amount | Payable non-held amount that is expected to settle in 13 days in system currency.
payable_14_day_total.seller_amount | Payable non-held amount that is expected to settle in 14 days in seller currency.
payable_14_day_total.system_amount | Payable non-held amount that is expected to settle in 14 days in system currency.
payable_gte_15_day_total.seller_amount | Payable non-held amount that is expected to settle in greater than or equal to 15 days in seller currency.
payable_gte_15_day_total.system_amount | Payable non-held amount that is expected to settle in greater than or equal to 15 days in system currency.
payable_total.seller_amount | Payable total in seller currency. This includes all payable totals that are not in transit.
payable_total.system_amount | Payable total in system currency. This includes all payable totals that are not in transit.
payment_group | Grouping of payments to seller. In some systems we group payments so that we can have separate remittance statments e.g. in Avfuel we have proprietry card payments and bank card payments grouped separately. Omit if not supported.
payment_method | Current payment method seller would be paid by e.g. EFT, Check, Wire.
pending_total.seller_amount | Payable total not including paid in transit or held amounts in seller currency. This should equal the sum of all seller payable buckets.
pending_total.system_amount | Payable total not including paid in transit or held amounts in system currency. This should equal the sum of all system payable buckets.
reimbursement_cycle_list | Current comma separated list of reimbursement payment cycles setup for seller.
seller_id | Originating systems unique identifier for seller.

## Notes
