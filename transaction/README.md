# Transaction Message Payload Definition

JSON Name | Comment
--- | ---
airline\_code | The airline code associated with the transaction if it was originated by an air travel agent
ap\_in\_dispute | Is this transaction in dispute in accounts payable system? Y/N Column Name: In Dispute? (Y/N)
ap\_only\_tax\_total.secondary\_amount | The total tax amount that affects the AP and does not affect the AR in client currency.
ap\_only\_tax\_total.seller\_amount | The total tax amount that affects the AP and does not affect the AR in seller currency.
ap\_only\_tax\_total.system\_amount | The total tax amount that affects the AP and does not affect the AR in system currency.
ap\_only\_total.secondary\_amount | The total of non-service fee details that are not client billable and affect the AP and do not affect the AR in client currency. Column Name: Non Billable AP Only Total (Client Currency)
ap\_only\_total.seller\_amount | The total of non-service fee details that are not client billable and affect the AP and do not affect the AR in seller currency. Column Name: Non Billable AP Only Total (Merchant Currency)
ap\_only\_total.system\_amount | The total of non-service fee details that are not client billable and affect the AP and do not affect the AR in system currency. Column Name: Non Billable AP Only Total (System Currency)
ap\_po\_number | Purchase Order Number in accounts payable system. This is used by some systems to enforce PO purchase policies. Column Name: AP PO Number
ap\_processing\_date | Accounts payable system processing date.
ap\_received\_at | Date/Time this transaction first entered the accounts payable system.
ap\_seller\_count | Payable seller count (If this is > 1 then we are split reimbursing). Column Name: Payable Merchant Count
ap\_seller\_id | Max payable seller ID that AP information is assoicated to. Column Name: Max Payable Merchant ID
ap\_service\_fee\_total.secondary\_amount | Accounts payable service fee total in client currency. Column Name: AP Service Fee Total (Client Currency)
ap\_service\_fee\_total.seller\_amount | Accounts payable service fee total in seller currency. Column Name: AP Service Fee Total (Merchant Currency)
ap\_service\_fee\_total.system\_amount | Accounts payable service fee total in system currency. Column Name: AP Service Fee Total (System Currency)
ap\_tax\_total.secondary\_amount | The total tax amount that affects the AP in client currency.
ap\_tax\_total.seller\_amount | The total tax amount that affects the AP in seller currency.
ap\_tax\_total.system\_amount | The total tax amount that affects the AP in system currency.
ap\_total.secondary\_amount | Accounts payable total in client currency (Pending Payment, Paid and Adjusted details. Does not include Service Fees or client billable amounts). Column Name: AP Total (Client Currency)
ap\_total.seller\_amount | Accounts payable total in seller currency (Pending Payment, Paid and Adjusted details. Does not include service fees or client billable amounts). Column Name: AP Total (Merchant Currency)
ap\_total.system\_amount | Accounts payable total in system currency (Pending Payment, Paid and Adjusted details. Does not include Service Fees or client billable amounts). Column Name: AP Total (System Currency)
ap\_transaction\_type | Transaction type defined by the accounts payable system.
ap\_updated\_at | Last date/time the transaction was updated by the accounts payable system.
approval\_code | Approval code submitted by the seller. This should be the approval code the seller received from the authorizing party. Column Name: Approval Code
ar\_buyer\_count | Billable customer count (If this is > 1 then we are split billing). Column Name: Billing Customer Count
ar\_buyer\_id | Max billable customer ID. Column Name: Max Billing Customer ID
ar\_close\_date | The date when the AR status flipped from open to closed. It covers things like adjustments and pay apply.
ar\_in\_dispute | Is this transaction in dispute in accounts receivable system? Y/N Column Name: In Dispute? (Y/N)
ar\_only\_tax\_total.buyer\_amount | The total tax amount that affects the AR and does not affect the AP in buyer currency.
ar\_only\_tax\_total.secondary\_amount | The total tax amount that affects the AR and does not affect the AP in client currency.
ar\_only\_tax\_total.system\_amount | The total tax amount that affects the AR and does not affect the AP in system currency.
ar\_only\_total.buyer\_amount | The total of non-service fee details that are not client billable and affect the AR and do not affect the AP in customer currency. Column Name: Non Payable AR Only Total (Customer Currency)
ar\_only\_total.secondary\_amount | The total of non-service fee details that are not client billable and affect the AR and do not affect the AP in system currency. Column Name: Non Payable AR Only Total (Client Currency)
ar\_only\_total.system\_amount | The total of non-service fee details that are not client billable and affect the AR and do not affect the AP in system currency. Column Name: Non Payable AR Only Total (System Currency)
ar\_open\_total.buyer\_amount | The open total of the transaaction in customer currency. Column Name: Open Total (Customer Currency)
ar\_open\_total.secondary\_amount | The open total of the transaaction in client currency. Column Name: Open Total (Client Currency)
ar\_open\_total.system\_amount | The open total of the transaaction in system currency. Column Name: Open Total (System Currency)
ar\_po\_number | Purchase Order Number in accounts receivable system. This is used by some systems to enforce PO purchase policies. Column Name: AP PO Number
ar\_processing\_date | Accounts receivable system processing date.
ar\_received\_at | Date/Time this transaction first entered the accounts receivable system.
ar\_service\_fee\_total.buyer\_amount | Accounts receivable service fee total in customer currency (NOTE: Should only see billed receivable service fees in Cummins). Column Name: AR Service Fee Total (Customer Currency)
ar\_service\_fee\_total.secondary\_amount | Accounts receivable service fee total in client currency. Column Name: AR Service Fee Total (Client Currency)
ar\_service\_fee\_total.system\_amount | Accounts receivable service fee total in system currency. Column Name: AR Service Fee Total (System Currency)
ar\_state | The current ar state of the transaction. If it is fully paid will display as closed if there is an unpaid amount it will display as open. If the transaction has yet to be billed, it will display as Pending.  Column Name: AR State
ar\_tax\_total.buyer\_amount | The total tax amount that affects the AR in buyer currency.
ar\_tax\_total.secondary\_amount | The total tax amount that affects the AR in client currency.
ar\_tax\_total.system\_amount | The total tax amount that affects the AR in system currency.
ar\_total.buyer\_amount | Accounts receivable total in customer currency (Pending Billing, Billed, Paid and Adjusted details. Does not include service fees or client billable amounts). Column Name: AR Total (Customer Currency)
ar\_total.secondary\_amount | Accounts receivable total in client curreny (Pending Billing, Billed, Paid and Adjusted details. Does not include service fees or client billable amounts). Column Name: AR Total (Client Currency)
ar\_total.system\_amount | Accounts receivable total in system currency (Pending Billing, Billed, Paid and Adjusted details. Does not include service fees or client billable amounts). Column Name: AR Total (System Currency)
ar\_transaction\_type | Transaction type defined by the accounts receivable system.
ar\_updated\_at | Last date/time the transaction was updated by the accounts receivable system.
batch.id | ID of the batch the trx is associated with, if there is an associated batch. Column Name: Batch ID
batch.number | Merchant assigned batch reference. Column Name: Batch Number
batch.settle\_date | Date/Time batch was settled from seller. Used to determine if batch should make transaction processing cutoff. For electronic batches value is when we received the settlement file. For paper tickets it is the postmark date. Column Name: Batch Settle Timestamp
batch.total | Batch total submitted by seller in seller currency. Column Name: Batch Total (Merchant Currency)
batch.transaction\_count | Number of Transactions included in the batch. Column Name: Number of Transactions in Batch
bill\_date | Billing date for transaction. This is the date used to determine the due date based on customers payment terms. Column Name: Bill Date
bill\_date\_count | Bill date count (If this is > 1 then we are billing line items on different billing cycles). Column Name: Bill Date Count
bill\_due\_date | The date that the bill for this transaction is due. If there are multiple bill dates the first one is shown. Column Name: Bill Due Date
book\_date | Book Date (Date the accounts receivable was booked). Column Name: Book Date
buyer\_id | Customer that the card belongs to at time of transaction based on transaction date or the customer the transaction belongs to if the transaction is not card based.  If the transaction is a Bank Card purchasing card, the associated customer will populate this column. This can be null if it is a third party card where we do not know the customer. Column Name: Purchasing Customer ID
card\_entry\_mode | An English description of the POS entry mode for card data e.g. Keyed, Swipe track I, Swipe track II. Column Name: Card Entry Mode
card\_identifier | (May be masked.) The card identifier provided by the seller. This may be null if the transaction is at an account level. Account level transactions are generated by the processing system, e.g., volume rebates etc... Column Name: Card Number
card\_not\_present | Industry standard, assumption is that the card is present, if not, we must indicate as such.
card\_type | An English description for the card type. Column Name: Card Type
client\_ap\_total.secondary\_amount | The total of non-service fee details that are client billable and affect the AP in client currency. Column Name: Client Billable AP Total (Client Currency)
client\_ap\_total.seller\_amount | The total of non-service fee details that are client billable and affect the AP in seller currency. Column Name: Client Billable AP Total (Merchant Currency)
client\_ap\_total.system\_amount | The total of non-service fee details that are client billable and affect the AP in system currency. Column Name: Client Billable AP Total (System Currency)
client\_ar\_total.buyer\_amount | The total of non-service fee details that are client billable and affect the AR in customer currency. Column Name: Client Billable AR Total (Customer Currency)
client\_ar\_total.secondary\_amount | The total of non-service fee details that are client billable and affect the AR in client currency. Column Name: Client Billable AR Total (Client Currency)
client\_ar\_total.system\_amount | The total of non-service fee details that are client billable and affect the AR in system currency. Column Name: Client Billable AR Total (System Currency)
client\_id | Business Unit Network ID Number. Column Name: Network ID
client\_id\_legacy | The two character client (network) identifier
client\_reference\_id | Client provided universal unique identifier for the invoice.
cmid | Identifies what seller terminal the batch came from. Column Name: Terminal ID
currencies.buyer | Customer Currency. Column Name: Customer Currency
currencies.secondary | Client Currency. Column Name: Client Currency
currencies.seller | Merchant Currency. Column Name: Merchant Currency
currencies.system | System Currency - 3 Character currency code e.g. USD. Column Name: System Currency
distinct\_bill\_count | The number of different bill due dates for the transaction. Column Name: Distinct Bill Count
in\_dispute | Is this transaction in dispute? Y/N Column Name: In Dispute? (Y/N)
initiate\_pay\_out\_date | Initiate Date - Date this payment was initiated at the bank. This is usually 1 to 2 days prior to pay out date. Column Name: Max Initiate Date
invoice\_date | The date/time for this transaction. This will normally be in the time zone of the seller unless it is an internally generated transaction, those will be in system timezone. Column Name: Invoice Date
network\_name | Business Unit Network Name. Column Name: Network Name
pass\_through | Is this transaction pass through? Y/N (Pass through means the transaction is neither payble to the seller nor receivable from the customer). Column Name: Pass Through? (Y/N)
passenger\_name\_record\_reference | A reference to the passenger name record for the plane ticket if the transaction was originated by an air travel agent for a ticket purchase.
pay\_out\_date | The actual pay out date to the seller for this transaction. Column Name: Max Pay Out Date
pay\_out\_date\_count | Pay out date count (If this is > 1 then we are paying out line items on different cycles). Column Name: Pay Out Date Count
payment\_method | Payment method used to pay seller for this transaction. (If payable seller count > 1 then this may not reflect all seller payment methods for the given transaction). Column Name: Payment Method
po\_number | Purchase Order Number, this is used by some systems to enforce PO purchase policies. Column Name: PO Number
processing\_date | Processing date. This will be the day this processing run is for. Column Name: Processing Date
processing\_id | Transaction processing stream run that processed this batch. This will only be different to the load\_stream\_run\_id if the cutoff for the seller was not met so we loaded it but did not process it. Column Name: Processing History ID
processor\_fee.seller\_amount | Processor fee converted to seller currency. (If the processor payment currency is the same as seller currency then this is just the fee that the processor charged. If different then we back calculate the processor fee in seller currency by taking the Processor Total/Merchant Total * (Processor Fee) rounded to precision of seller's currency) (NOTE: Processor fee only supported in B4Flight-Avcard and AMEX). Column Name: Processor Fee (Payment Currency)
processor\_fee.system\_amount | Processor fee converted to system currency. (If the system currency is the same as the seller currency then this will be the same value as the del\_processor\_fee otherwise we convert the processor payment currency to system currency.) (NOTE: Processor fee only supported in B4Flight-Avcard and AMEX). Column Name: Processor Fee (System Currency)
program\_fee\_id | Max program fee ID applied to details. If program\_fee\_id\_count > 1 then this will not be representing all program fee IDs that were applied. Column Name: Max Program Fee ID
program\_fee\_id\_count | Count of distinct program fee IDs applied to the transaction. If this is > 1 then we are applying different program fees across details for that transaction. Column Name: Program Fee ID Count
program\_fee.secondary\_amount | Program fee in client currency. Column Name: Program Fee (Client Currency)
program\_fee.system\_amount | Program fee in system currency. Column Name: Program Fee (System Currency)
program\_id | The program under the client Apruve, e.g., Apruve.
projected\_pay\_out\_date | The projected pay out date for this transaction. This may be different than the actual pay out date due to the holding of payment. Column Name: Max Projected Pay Out Date
projected\_pay\_out\_date\_count | Projected pay out date count (If this is > 1 then we are projecting pay out dates for line items on different cycles). Column Name: Projected Pay Out Date Count
received\_at | Date/Time this batch first entered the system. This could be from settlegate, keyed or any other entry point for transactions. Column Name: Batch Entry Time
received\_mode | Identifies how batch entered the system e.g. keyed, settlement gateway etc. Column Name: Batch Entry Mode
revenue | The amount of revenue we made on this transaction.
seller\_id | Filled in with the seller business unit that delivered the batch of transactions. This could be assigned based on the cm\_id from the batch or assigned based on the third party processor that sent us the batch/transactions.  Not all transactions (e.g. internally generated) will be associated with a Batch. Column Name: Purchasing Merchant ID
seller\_payment\_method | Merchant Payment Method. Column Name: Merchant Payment Method
seller\_reference | Transaction reference assigned by seller and usually the one printed on the customer's ticket. Column Name: Invoice Number
service\_type | Transaction Service Type e.g. Labor Only, Parts Only. Column Name: Service Type
statement\_number | The statement number show on the bill for this transaction
submitted\_tax\_total.buyer\_amount | Submitted tax total by seller in customer currency.
submitted\_tax\_total.secondary\_amount | Submitted tax total by seller in client currency.
submitted\_tax\_total.seller\_amount | Submitted tax total by seller in seller currency.
submitted\_tax\_total.system\_amount | Submitted tax total by seller in system currency.
submitted\_total.buyer\_amount | Submitted total in customer currency (This will include payable and pass through amounts). Column Name: Submitted Total (Customer Currency)
submitted\_total.secondary\_amount | Submitted total in client currency (This will include payable and pass through amounts). Column Name: Submitted Total (Client Currency)
submitted\_total.seller\_amount | Submitted total in seller currency (This will include payable and pass through amounts) (NOTE: Will include any pre-processing amounts e.g. Avalara Taxes). Column Name: Submitted Total (Merchant Currency)
submitted\_total.system\_amount | Submitted total in system currency (This will include payable and pass through amounts). Column Name: Submitted Total (System Currency)
ticket\_number | The ticket number associated with the transaction if the transaction was originated by an air travel agent
transaction\_id | Transaction ID. Column Name: Transaction ID
transaction\_listing\_number | The listing file number associated with the transaction if the transaction was originated by an air travel agent
transaction\_status | Status of transaction Processed, Rejected. Everything in the fr\_transactions table will be Processed. Column Name: Transaction Status
transaction\_type | Description of the transaction type of transaction e.g. Sale, Credit, Late Fee, Adjustment, Account Charge. Column Name: Transaction Type
travel\_agent\_sub\_account\_id | The travel agent's sub-account identifier if the transaction was originated by an air travel agent
updated\_at | The date this transaction was updated.


## Notes

### Business Model Fee/Total Amount Notes

| **Business Model** | **submitted_total** | **ar_total** | **ar_only_total** | **client_ar_total** | **ap_total** | **ap_only_total** | **ap_service_fee_total** | **ar_service_fee_total** | **program_fee_total** | **processor_fee_total** | **ar_open_total** | **client_ap_total** | **Comment** | **Projects** |
| ------------------ | ------------------- | ------------ | ----------------- | ------------------- | ------------ | ----------------- | ------------------------ | ------------------------ | --------------------- | ----------------------- | ----------------- | ------------------- | ----------- | ------------ |
| **Client** | Total submitted by Merchant. | Total billable to customer not including Client or Service Fees. | Total billable to customer not payable. Funded by AP/AR owner. | Total billable funded by Client. (Does not include service fees) | Total payable not including Client or Service Fees. | Total Payable that is not billable. Funded by AP/AR owner. Not including Client or Service Fees. | Payable Service Fee deducted from Merchant. Client funded. | Service fee billed to the seller instead of deducting from payable.  | Fee billed outside system to Client by FinCon.  | N/A |  | Total payable funded by Client. Not including Service Fees. | Revenue is the Program Fee plus or minus any AP/AR only funded totals. | Carrier, DAF, eVIVA (Great Dane), GM Fleet, Navistar, PACCAR, Red Wing |
| **Pass Through** | Total submitted by Merchant. | None | None | None | None | None | None | None | Fee billed outside system to Client by FinCon.  | N/A |  | None | Revenue is the Program Fee. | AIR Card, SEA Card, Avfuel, AvBizDash, PACCAR |
| **Proprietary** | Total submitted by Merchant. | Total billable to customer not inlcuding Service Fees. | AP/AR owner funded fee/discount to customer added at processing time.  | N/A | Total payable not including Service Fee. | AP/AR owner funded fees/discounts to seller added at processing time. Not including Service Fee. | Payable Service Fee deducted from Merchant's payable. | Service fee billed to the seller instead of deducting from payable.  | N/A | N/A |  | N/A | Revenue is the Service Fee plus or minus any AP/AR only funded totals. | AIR Card, SEA Card, Fuel Card, MSE TTT (Note TTT doesn't charge Service Fee but adds a customer fee in AR Only) |
| **Third Party** | Total submitted by Merchant. | Total billable to customer. (Processor) | N/A | N/A | Total payable not including Service Fee. | AP/AR owner funded fees/discounts to seller added at processing time. Not including Service Fee. | Payable Service Fee deducted from Merchant's payable. | Service fee billed to the seller instead of deducting from payable.  | N/A | Fee charged to AP/AR owner for processing third party cards. |  | N/A | Revenue is the Merchant Service fee minus the Processor Fee plus or minus any AP only funded totals. | AvBizDash, Avfuel (Third Party), Navistar |
| **Client Hybrid**  | Total submitted by Merchant. | Total billable to customer not including Client billable. | Total billable to customer not payable. Funded by AP/AR owner. | Total billable funded by Client. (Does not include service fees) | Total payable not including Client or Service Fees. | Total Payable that is not billable. Funded by AP/AR owner. Not including Client or Service Fees. | Payable Service Fee deducted from Merchant's payable. | N/A | N/A | N/A |  | Totall payable funded by Client. Not including Service Fees. | "All sellers set up as children to seller representing client. The system calculates fee to client that represents the "Program Fee". So revenue is AP Service fee plus or minus any AP/AR only funded totals." | Avfuel (Avfuel Cards), Best Buy, eVIVA (Cummins) |

