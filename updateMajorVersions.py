import sys
import re

# Removes minor from $id for major-only tags.
# So v1 would have an $id referencing /v1/schema.json even though it's const version field would be 1.x
if len(sys.argv[1]) == 2:
    with open(sys.argv[2] + "/schema.json", "r") as file:
        data = file.read()
        regex = r"/v[0-9][.][0-9]/"
        subst = "/" + sys.argv[1] + "/"
        result = re.sub(regex, subst, data)
    with open(sys.argv[2] + "/schema.json", "w") as write_file:
        write_file.write(result)