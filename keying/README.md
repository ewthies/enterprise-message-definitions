# Dispute Message Payload Definition

This message handles metadata around keying statistics. Each message should contain data for a single keyed transaction.

Required Fields
* client_id
* program_id
* transaction_id

JSON Name | Comment
--- | ---
client_id | For GUESS projects this is analogous to the Network. So for 'PACCAR - Mexico' they will submit 'PM'. The network/client Cummins would submit 'CU'. For Australia projects, this will be the ID for the client, e.g., 74243728-8819-4cc0-9585-774e2ab1e7fc as the ID for Apruve. This value cannot change and must be unique.
client_id_legacy | For ALL projects this will be the 2-letter Network Identifier. For TAP this is 'TA', for B2BC this is 'TI'.
created_at | Date/Time merchant invoice documents were identified as needing to be keyed.
detail_count | Number of line item details keyed under the transaction.
key_user | Person who keyed the transaction into the system from prepped merchant invoice document.
keyed_date | Date/Time the keyer finished keying transaction and made ready to be pulled into processing.
original_received_date | Date/Time the invoice documentation was received from the Seller.
prep_user | Person who prepped the seller invoice documents for the keyer. 
prepped_date | Date/Time merchant invoice documents were finished being prepped and made available for a keyer to key.
processing_date | Processing date the keyed transaction was processed on.
program_id | For GUESS projects, this will be the Schema name, e.g., DAF. For Australia projects, this will be the ID for the program, e.g, e2c67ae4-39d9-4220-95b2-d1335d83d5d7 for 4MD Premium. This value cannot change and must be unique.
seller_reference | Transaction reference assigned by merchant and usually the one printed on the buyer's invoice/ticket. 
transaction_id | The transaction ID that was keyed.
transaction_status | Status of keyed transaction.


