{
	"$schema": "http://json-schema.org/draft-07/schema#",
	"$id": "https://enterprise-messaging.msts.com/schemas/keying/v1.0/schema.json",
	"definitions": {
		"originSystemIdentifier": {
			"$id": "#/definitions/originSystemIdentifier",
			"type": "string",
			"title": "The Origin System Identifier Schema",
			"description": "An ID generated in a MSTS owned system that is the origin of the relevant data",
			"examples": [
				"2dec5b9d-f37f-4733-964e-8e5378c896fb"
			],
			"maxLength": 50
		}
	},
	"type": "object",
	"title": "The MSTS Keying Schema",
	"required": [
		"created_at",
		"id",
		"origin",
		"payload",
		"type",
		"version"
	],
	"properties": {
		"created_at": {
			"$id": "#/properties/created_at",
			"type": "string",
			"title": "The Created_at Schema",
			"description": "When this message was created.",
			"examples": [
				"2018-10-11T03:34:57.039854+00:00"
			],
			"format": "date-time"
		},
		"id": {
			"$id": "#/properties/id",
			"title": "The Id Schema",
			"description": "The Unique ID of this message from the producer's perspective.",
			"allOf": [{
				"$ref": "#/definitions/originSystemIdentifier"
			}]
		},
		"origin": {
			"$id": "#/properties/origin",
			"type": "string",
			"title": "The Origin Schema",
			"description": "The producer of this message.",
			"examples": [
				"BESTBUY"
			],
			"pattern": "^(.*)$",
			"maxLength": 45
		},
		"type": {
			"$id": "#/properties/type",
			"type": "string",
			"title": "The Type Schema",
			"description": "The standard entity name for this format.",
			"const": "keying",
			"examples": [
				"keying"
			]
		},
		"version": {
			"$id": "#/properties/version",
			"type": "string",
			"title": "The Version Schema",
			"description": "The version of this message format.",
			"const": "1.0",
			"examples": [
				"1.0"
			]
		},
		"payload": {
			"$id": "#/properties/payload",
			"type": "object",
			"title": "The Payload Schema",
			"description": "The details for this message.",
			"required": [
				"client_id",
				"program_id",
                "client_id_legacy",
				"transaction_id"
			],
			"properties": {
				"client_id": {
					"$id": "#/properties/payload/properties/client_id",
					"title": "The Client_id Schema",
					"allOf": [{
						"$ref": "#/definitions/originSystemIdentifier"
					}]
				},
				"client_id_legacy": {
					"$id": "#/properties/payload/properties/client_id_legacy",
					"title": "The Client_id_legacy Schema",
					"allOf": [{
						"$ref": "#/definitions/originSystemIdentifier"
					}]
				},
				"created_at": {
					"$id": "#/properties/payload/properties/created_at",
					"type": "string",
					"title": "The Created_at Schema",
					"examples": [
						"2018-10-12T11:23:44.000000-06:00"
					],
					"format": "date-time"
				},
				"detail_count": {
					"$id": "#/properties/payload/properties/detail_count",
					"type": "integer",
					"title": "The Detail_count Schema",
					"examples": [
					    3
					]
				},
				"key_user": {
					"$id": "#/properties/payload/properties/key_user",
					"type": "string",
					"title": "The Key_user Schema",
					"examples": [
						"ABENEA"
					],
					"pattern": "^(.*)$",
					"maxLength": 200
				},
				"keyed_date": {
					"$id": "#/properties/payload/properties/keyed_date",
					"type": "string",
					"title": "The Keyed_date Schema",
					"examples": [
						"2018-10-12T11:23:44.000000-06:00"
					],
					"format": "date-time"
				},
				"original_received_date": {
					"$id": "#/properties/payload/properties/original_received_date",
					"type": "string",
					"title": "The Original_received_date Schema",
					"examples": [
						"2018-10-12T11:23:44.000000-06:00"
					],
					"format": "date-time"
				},
				"prep_user": {
					"$id": "#/properties/payload/properties/prep_user",
					"type": "string",
					"title": "The Prep_user Schema",
					"examples": [
						"ABENEA"
					],
					"pattern": "^(.*)$",
					"maxLength": 200
				},
				"prepped_date": {
					"$id": "#/properties/payload/properties/prepped_date",
					"type": "string",
					"title": "The Prepped_date Schema",
					"examples": [
						"2018-10-12T11:23:44.000000-06:00"
					],
					"format": "date-time"
				},
				"processing_date": {
					"$id": "#/properties/payload/properties/processing_date",
					"type": "string",
					"title": "The Processing_date Schema",
					"examples": [
						"2018-10-12T11:23:44.000000-06:00"
					],
					"format": "date-time"
				},
				"program_id": {
					"$id": "#/properties/payload/properties/program_id",
					"title": "The Program_id Schema",
					"allOf": [{
						"$ref": "#/definitions/originSystemIdentifier"
					}]
				},
				"seller_reference": {
					"$id": "#/properties/payload/properties/seller_reference",
					"title": "The Seller_reference Schema",
					"allOf": [{
						"$ref": "#/definitions/originSystemIdentifier"
					}]
				},
				"transaction_id": {
					"$id": "#/properties/payload/properties/transaction_id",
					"title": "The Transaction_id Schema",
					"allOf": [{
						"$ref": "#/definitions/originSystemIdentifier"
					}]
				},
				"transaction_status": {
					"$id": "#/properties/payload/properties/transaction_status",
					"type": "string",
					"title": "The Transaction_status Schema",
					"examples": [
						"Processed"
					],
					"pattern": "^(.*)$",
					"maxLength": 200
				}
			}
		}
	}
}
