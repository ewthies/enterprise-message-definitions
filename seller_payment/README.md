# Seller Payment Message Payload Definition
This message handles Seller Payments for the enterprise. Each message should contain data for a single payment to the seller.
The transaction_total plus the transaction_fee_total plus the adjustment_total plus the adjustment_fee_total should equal the payment_total in all currencies.

Required Fields
* client_id
* client_id_legacy
* currencies
    * seller
    * secondary
    * system
* payment_method
* payment_total
    * seller_amount
    * secondary_amount
    * system_amount
* program_id
* remit_id
* seller_id

JSON Name | Comment
--- | ---
adjustment_fee_total.seller_amount | The total adjusted service fee amount of this payment in seller currency. Display Name: AP Adjustment Service Fee Total (Seller Currency)
adjustment_fee_total.secondary_amount | The total adjusted service fee amount of this payment in client currency. Display Name: AP Adjustment Service Fee Total (Client Currency)
adjustment_fee_total.system_amount | The total adjusted service fee amount of this payment in system currency. Display Name: AP Adjustment Service Fee Total (System Currency)
adjustment_total.seller_amount | The total adjustment amount of this payment in seller currency not including service fees. Display Name: AP Adjustment Total (Seller Currency)
adjustment_total.secondary_amount | The total adjustment amount of this payment in client currency not including service fees. Display Name: AP Adjustment Total (Client Currency)
adjustment_total.system_amount | The total adjustment amount of this payment in system currency not including service fees. Display Name: AP Adjustment Total (System Currency)
banking_country | The banking country of the seller. This is the ISO 3166-1 alpha-2. Display Name: Banking Country
bank_account_last_4 | The last for digits of the seller bank account number used for payment. Display Name: Bank Account Number (Last 4)
card_type_group | The card type group that is associated with the payment e.g. Proprietary, Bank Card. Only valid for GUESS systems. Display Name: Card Type Group
clearing_house | The banking institution that was used for clearing payment to the seller. Display Name: Clearing House
client_id | For GUESS projects this is analogous to the Network. So for 'PACCAR - Mexico' they will submit 'PM'. The network/client Cummins would submit 'CU'. For Australia projects, this will be the ID for the client, e.g., 74243728-8819-4cc0-9585-774e2ab1e7fc as the ID for Apruve. This value cannot change and must be unique.
client_id_legacy | For ALL projects this will be the 2-letter Network Identifier. So 'PACCAR - Australia' would submit 'PA', PACCAR would submit 'PP', Cummins would submit 'CU'.
currencies.seller | Seller currency - 3 character currency code e.g. USD. Display Name: Seller Currency
currencies.secondary | Client Currency - 3 Character currency code e.g. USD. Display Name: Client Currency
currencies.system | System Currency - 3 character currency code e.g. USD. Display Name: System Currency
initiate_date | Date included in bank clearing house file instructing when to initiate payment. We will be set up with 1 or 2 day pay that will determine pay out date. Display Name: Initiate Date
payment_method | The payment method used to pay the seller e.g. Check, Wire, ACH. Display Name: Payment Method
payment_status | The status of the payment e.g. Paid, in-transit. Display Name: Payment Status
payment_total.seller_amount | The total amount of this payment in seller currency. Display Name: AP Payment Total (Seller Currency)
payment_total.secondary_amount | The total amount of this payment in client currency. Display Name: AP Payment Total (Client Currency)
payment_total.system_amount | The total amount of this payment in system currency. Display Name: AP Payment Total (System Currency)
payout_date | Date the payment is intended to be paid to the seller. Display Name: Payout Date
processing_date | Processing date that pulled this payment to be sent to the clearing house. For B2BC/TAP this should be the reporting cutoff.
processing_id | System identifier of the processing run that pulled this payment to be sent to the clearing house. Display Name: Processing History ID
program_id | The program under the client Apruve, e.g., Apruve.
remit_id | Unique payment identifier assigned by processing system. Display Name: Remit ID
resent | Was this payment resent to the bank Y/N aka true/false. This is usually done when banking information is incorrect and payment needs to be resent. Display Name: Resent (Y/N)
seller_id | Seller ID assigned by the system. Display Name: Seller ID
transaction_total.seller_amount | The total transaction amount of this payment in seller currency not including service fees. Display Name: AP Transaction Total (Seller Currency)
transaction_total.secondary_amount | The total transaction amount of this payment in client currency not including service fees. Display Name: AP Transaction Total (Client Currency)
transaction_total.system_amount | The total transaction amount of this payment in system currency not including service fees. Display Name: AP Transaction Total (System Currency)
transaction_fee_total.seller_amount | The total transaction service fee amount of this payment in seller currency. Display Name: AP Transaction Service Fee Total (Seller Currency)
transaction_fee_total.secondary_amount | The total transaction service fee amount of this payment in client currency. Display Name: AP Transaction Service Fee Total (Client Currency)
transaction_fee_total.system_amount | The total transaction service fee amount of this payment in system currency. Display Name: AP Transaction Service Fee Total (System Currency)
unipay_id | The payment ID for this payment in the uniPAY system. Display Name: uniPAY ID

## Notes
