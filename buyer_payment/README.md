# Buyer Payment Message Payload Definition
This message handles Buyer Payments for the enterprise. Each message should contain data for a single payment.

Required Fields
* buyer_id
* client_id
* client_id_legacy
* currencies
    * buyer
    * secondary
    * system
* deposit_date
* pay_id
* pay_total
    * buyer_amount
    * secondary_amount
    * system_amount
* payment_method
* program_id

JSON Name | Comment
--- | ---
adjusted_total.buyer_amount | The total adjusted amount of this payment in customer currency. Column Name: Adjusted Total (Customer Currency)
adjusted_total.secondary_amount | The total adjusted amount of this payment in client currency. Column Name: Adjusted Total (Client Currency)
adjusted_total.system_amount | The total adjusted amount of this payment in system currency. Column Name: Adjusted Total (System Currency)
adjustment_count | Number of times the payment was adjusted. This contributes to the touch count and includes payment transfers and adjustment backouts. Column Name: Adjustment Count
applied_total.buyer_amount | The total amount of transactions paid by this payment in customer currency. Column Name: Applied Total (Customer Currency)
applied_total.secondary_amount | The total amount of transactions paid by this payment in client currency. Column Name: Applied Total (Client Currency)
applied_total.system_amount | The total amount of transactions paid by this payment in system currency. Column Name: Applied Total (System Currency)
bill_date | Billing date this payment first showed up as a payment received to customer. Column Name: Bill Date
book_date | Book Date is the date the payment was booked to the accounts receivable. This will be on the first bill run after posting payment. Column Name: Book Date
buyer_id | Customer ID assigned by the system. Column Name: Customer ID
client_id | For GUESS projects this is analogous to the Network. So for 'PACCAR - Mexico' they will submit 'PM'. The network/client Cummins would submit 'CU'. For Australia projects, this will be the ID for the client, e.g., 74243728-8819-4cc0-9585-774e2ab1e7fc as the ID for Apruve. This value cannot change and must be unique.
client_id_legacy | For ALL projects this will be the 2-letter Network Identifier. So 'PACCAR - Australia' would submit 'PA', PACCAR would submit 'PP', Cummins would submit 'CU'.
compliance_decision_comment.author | Compliance User who approved or denied a payment
compliance_decision_comment.date | Date the compliance user commented on the payment.
compliance_decision_comment.text | A comment from compliance user regarding their decision
currencies.buyer | Customer currency - 3 character currency code e.g. USD. Column Name: Customer Currency
currencies.client | Client Currency - 3 Character currency code e.g. USD. Column Name: Client Currency
currencies.system | System Currency - 3 character currency code e.g. USD. Column Name: System Currency
deposit_date | Date that the payment was deposited into the bank account. Column Name: Deposit Date
entry_method | Method that the payment was entered into the system e.g. Manually Keyed, Direct Debit, Online Payment. Column Name: Entry Method
entered_at | Date/time the payment was received/entered into the system. Column Name: Received Date
entry_user | User that entered the payment into the system. Column Name: Entry User
full_apply_date | Date the payment was fully applied leaving zero unappled amount. Column Name: Full Apply Date
initial_apply_date | Date the payment was initially applied to one or more transactions. Column Name: Initial Apply Date
initial_apply_entry_mode | The process by which the pay info was entered into the system, for the first application. This will only be populated when the payment method is Pay Info Column Name: Initial Apply Entry Mode
initial_apply_entry_source | The Email Address of the user who sent the Pay Info or who entered the batch pay, or who performed the manual apply for the first payment application, for Auto-Apply, this will be null. Column Name: Initial Apply Email Address
initial_apply_method | Inital way in which the payment was applied to transactions e.g. Auto Apply, Manual Apply, Pay Info. Column Name: Initial Apply Method
initial_apply_process | The process that first ran the application. Column Name: Initial Apply Entry Source
initial_attempt_date | Date the payment was initially attempted on one or more transactions. Column Name: Initial Attempt Date
initial_attempt_entry_mode | The process by which the pay info was entered into the system, for the first application attempt. This will only be populated when the payment method is Pay Info Column Name: Initial Attempt Entry Mode
initial_attempt_entry_source | The Email Address of the user who sent the Pay Info or who entered the batch pay, or who performed the manual apply for the first payment application attempt, for Auto-Apply, this will be null. Column Name: Initial Attempt Email Address
initial_attempt_method | Initial way in which the payment was attempted to be applied to transactions e.g. Auto Apply, Manual Apply, Pay Info. Column Name: Initial Attempt Method
initial_attempt_process | The process that first ran the application attempt. Column Name: Initial Attempt Entry Source
last_apply_entry_mode | The process by which the pay info was entered into the system, for the last application. This will only be populated when the payment method is Pay Info Column Name: Last Apply Entry Mode
last_apply_entry_source | The Email Address of the user who sent the Pay Info or who entered the batch pay, or who performed the manual apply for the last payment application, for Auto-Apply, this will be null. Column Name: Last Apply Email Address
last_apply_method | Last way in which the payment was applied to transactions e.g. Auto Apply, Manual Apply, Pay Info. If there are multiple apply methods then they will be listed in the order that they happened. Column Name: Last Apply Method
last_apply_process | The process that last ran the application. Column Name: Last Apply Entry Source
manual_apply_count | Number of times the payment was manually applied. This contributes to the touch count. Column Name: Manual Apply Count
network_name | Customers network name. Column Name: Network Name
pay_file_id | Identifies the batch of payments that this payment was received/keyed in. Column Name: Payment File ID
pay_id | Internal payment identifier. Column Name: Payment ID
pay_total.buyer_amount | The total of the payment submitted by the customer in customer currency. This will be zero for transfer payments and zero lock box payments. Column Name: Payment Total (Customer Currency)
pay_total.secondary_amount | The total of the payment submitted by the customer in client currency. This will be zero for transfer payments and zero lock box payments. Column Name: Payment Total (Client Currency)
pay_total.system_amount | The total of the payment submitted by the customer in system currency. This will be zero for transfer payments and zero lock box payments. Column Name: Payment Total (System Currency)
payment_method | The method that the customer paid in e.g. Check, EFT, Wire. Column Name: Payment Method
payment_number | Payment reference number e.g. check number. Column Name: Payment Number
payment_touch_count | Number of human interactions with a payment. This is calculated by adding 1 if the payment was keyed plus number of manual payment applications (includes Pay Info and Manual Apply) plus number of adjustments performed on the payment. The apply and adjustment counts includes person backing out. Since we do not have information defining if a Pay Info is done automatically by uniPAY or manually then we will currently consider all Pay Info in the Payment Touch Count. Column Name: Payment Touch Count
payment_type | Regular payment types are payments sent in by the customer. Transfer payments are created when we transfer money from a payment to another customer account and Zero payments are keyed by accounting and used to clear credit/debit transactions that foot to zero. Column Name: Payment Type
posted_date | Date payment was pulled in by pay apply in reconcile mode and posted to customers accounts receivable Column Name: Posted Date
program_id | The program under the client Apruve, e.g., Apruve.
reconcile_date | Date that the payment file was reconciled with the bank making the payments ready to be posted to customer accounts. Column Name: Reconcile Date
status | A human readable string for the status of the payment.
transfer_from_buyer_id | If the payment is a Transfer type payment then this is the customer ID of the payment the money was tranferred from. Column Name: Transfer Customer
transfer_from_pay_id | If the payment is a Transfer type payment then this is the pay ID of the payment the money was tranferred from. Column Name: Transfer Payment ID
transfer_to_count | How many times this payment has been transferred to other customer accounts. This count does not include transfer backouts. Column Name: Transfer Count
travel_agent_sub_account_id | The travel agent's sub-account identifier if the payment was originated by an air travel agent
travel_agent_transfer_id | The transfer identifier associated with the payment if it was originated by an air travel agent
travel_agent_user_id | The travel agent's identifier if the payment was originated by an air travel agent
uc_aging_1_7.count | This will be 1 if the payment has an unapplied amount aged in this day range and 0 if it is not. Column Name: Aging Unapplied Count 1-7 Days
uc_aging_1_7.system_amount | Unapplied credit aging in system currency. Total unapplied amount deposited between 1 and 7 days. Column Name: Aging Unapplied Total 1-7 Days (System Currency)
uc_aging_180_269.count | This will be 1 if the payment has an unapplied amount aged in this day range and 0 if it is not. Column Name: Aging Unapplied Count 180-269 Days
uc_aging_180_269.system_amount | Unapplied credit aging in customer currency. Total unapplied amount deposited between 180 and 269 days. Column Name: Aging Unapplied Total 180-269 Days (System Currency)
uc_aging_270_359.count | This will be 1 if the payment has an unapplied amount aged in this day range and 0 if it is not. Column Name: Aging Unapplied Count 270-359 Days
uc_aging_270_359.system_amount | Unapplied credit aging in customer currency. Total unapplied amount deposited between 270 and 359 days. Column Name: Aging Unapplied Total 270-359 Days (System Currency)
uc_aging_30_59.count | This will be 1 if the payment has an unapplied amount aged in this day range and 0 if it is not. Column Name: Aging Unapplied Count 30-59 Days
uc_aging_30_59.system_amount | Unapplied credit aging in customer currency. Total unapplied amount deposited between 30 and 59 days. Column Name: Aging Unapplied Total 30-59 Days (System Currency)
uc_aging_360_plus.count | This will be 1 if the payment has an unapplied amount aged in this day range and 0 if it is not. Column Name: Aging Unapplied Count 360+ Days
uc_aging_360_plus.system_amount | Unapplied credit aging in customer currency. Total unapplied amount deposited 360 days or more. Column Name: Aging Unapplied Total 360+ Days (System Currency)
uc_aging_60_89.count | This will be 1 if the payment has an unapplied amount aged in this day range and 0 if it is not. Column Name: Aging Unapplied Count 60-89 Days
uc_aging_60_89.system_amount | Unapplied credit aging in customer currency. Total unapplied amount deposited between 60 and 89 days. Column Name: Aging Unapplied Total 60-89 Days (System Currency)
uc_aging_8_29.count | This will be 1 if the payment has an unapplied amount aged in this day range and 0 if it is not. Column Name: Aging Unapplied Count 8-29 Days
uc_aging_8_29.system_amount | Unapplied credit aging in customer currency. Total unapplied amount deposited between 8 and 29 days. Column Name: Aging Unapplied Total 8-29 Days (System Currency)
uc_aging_90_179.count | This will be 1 if the payment has an unapplied amount aged in this day range and 0 if it is not. Column Name: Aging Unapplied Count 90-179 Days
uc_aging_90_179.system_amount | Unapplied credit aging in customer currency. Total unapplied amount deposited between 90 and 179 days. Column Name: Aging Unapplied Total 90-179 Days (System Currency)
unapplied_total.buyer_amount | The total unapplied amount of the payment in customer currency. Column Name: Unapplied Total (Customer Currency)
unapplied_total.secondary_amount | The total unapplied amount of the payment in client currency. Column Name: Unapplied Total (Client Currency)
unapplied_total.system_amount | The total unapplied amount of the payment in system currency. Column Name: Unapplied Total (System Currency)
unipay_payment_id | Identifier for a bank payment within uniPAY. 1:1 mapping with a receivable (payment) sent to the company's bank account.
unipay_payment_initiation_id | Identifier for the payment initiation request in uniPAY.
unipay_post_decision_id | Identifier for a customer payment within uniPAY. Multiple customer payments may be split from a single bank payment. 1:1 mapping with an allocation of funds that is sent to a project/program as a payment.
updated_at | The most recent datetime this buyer_payment was updated in the origin system

## Notes

