# Enterprise Message Payload Definitions

## Overview

MSTS has several standard entities that:
* Exist in multiple platforms/services/systems (producers)
* Need to be communicated to other platforms/services/systems (consumers)

See [Enterprise Message Overview] for more detail

## Common Requirements
1. Message formats are consumer-neutral:
   1. For most entities they are the raw data from the source system in normalized format.
   2. Consumers are responsible for any aggregation unless otherwise noted.
2. When the subject of the message (e.g. Buyer, Seller, etc.) changes, the entire structure **must** be re-published since the producer can make no assumption on what 'version' a particular consumer previously had (if any). 
3. All dates and timestamp values **must** be formatted as per ISO 8601 using the following format:
   1. YYYY-MM-DDTHH:MI:SS.FFFFFF+HH:MI (this matches the [`"format": "date-time"` JSON-schema validation])
4. All currency amounts **must** be decimal values in the smallest currency unit, for instance:
   1. AUD = 1.00
   2. CAD = -47.50
   3. USD = 13.22
5. All phone numbers are formatted as per [E.164 Standard]
   1. Leading '+' **must** always be present.
   2. All whitespace **must** be removed.
   3. Extension to be entered after connection **may**  be represented with ',' followed by the extension number.
   4. Examples:
      1. Call Chris's mobile = +18167180468
      2. Call Steve's mobile = +61412151200
      2. Call Steve's desk via the main office number = +61396782200,2255
6. Each message **must** have the following header values:
   1. created_at = timestamp this message was created at
   2. type = <standard entity - see below>
   3. version = <version of standard entity>
   3. id = unique id of this message from the producer's perspective
   4. origin = producer platform/service/system (system of record for this message)
   5. payload = <per standard entity - see below>
7. Having multiple producers and consumers means a high cost of implementing new versions
   1. Main goal is to make evolutionary, non-breaking changes - stay at version = 1.0
   2. Any consideration of breaking changes **must** include discussion and agreement of directors
8. All messages **must** be validated against the schemas in AWS before being sent or consumed to ensure that everyone is validating off the same version of the schema.
9. All messages **must** de defined with  `"additionalProperties": false` to ensure that only properties that are defined in the schema are considered valid for the purpose of validation.
10. Consumers **can** infer uniqueness of a message via the combination of origin, type, and id
   1. **May** use to identify whether or not a message has previously been received
   2. **May** choose to drop/ignore or take an alternate code path
11. Consumers **can** use created_at to determine whether they've received an out of order message
   1. **May** choose to drop/ignore older messages when a newer one has already been received

## Tools
* Used [jsonschema.net] to infer and edit schema

## Example Payload Wrapper

```json
{  
    "created_at": "2018-10-11T03:34:57.039854+00:00",
    "type": "transaction",
    "version": "1.0",
    "id": "609791c9-eac4-41ac-9403-df083c2b4482",
    "origin": "B2BC",
    "payload": { }
}
```

## Standard Entities

| **Entity/Message** | **Development SNS Topic** | **Beta SNS Topic** | **Production SNS Topic** |
| ------------------ | --------------------- | -------------- | -------------------- |
| [accounts_payable](/accounts_payable) | [Bus_Development_Accounts_Payable] | [Bus_Beta_Accounts_Payable] | [Bus_Production_Accounts_Payable] |
| [accounts_receivable](/accounts_receivable) | [Bus_Development_Accounts_Receivable] | [Bus_Beta_Accounts_Receivable] | [Bus_Production_Accounts_Receivable] |
| [adjustment](/adjustment) | [Bus_Development_Adjustment]| [Bus_Beta_Adjustment] | [Bus_Production_Adjustment] |
| [buyer](/buyer) | [Bus_Development_Buyer] | [Bus_Beta_Buyer] | [Bus_Production_Buyer] |
| [buyer_application](/buyer_application) | [Bus_Development_Buyer_Application] | [Bus_Beta_Buyer_Application] | [Bus_Production_Buyer_Application] |
| [buyer_card](/buyer_card) | [Bus_Development_Buyer_Card] | [Bus_Beta_Buyer_Card] | [Bus_Production_Buyer_Card] |
| [buyer_credit_increase](/buyer_credit_increase) | [Bus_Development_Buyer_Credit_Increase] | [Bus_Beta_Buyer_Credit_Increase] | [Bus_Production_Buyer_Credit_Increase] |
| [buyer_payment](/buyer_payment) | [Bus_Development_Buyer_Payment] | [Bus_Beta_Buyer_Payment] | [Bus_Production_Buyer_Payment] |
| [contact](/contact) | [Bus_Development_Contact] | [Bus_Beta_Contact] | [Bus_Production_Contact] |
| [dispute](/dispute) | [Bus_Development_Dispute] | [Bus_Beta_Dispute] | [Bus_Production_Dispute] |
| [keying](/keying) | [Bus_Development_Keying] | [Bus_Beta_Keying] | [Bus_Production_Keying] |
| [product](/product) | [Bus_Development_Product] | [Bus_Beta_Product] | [Bus_Production_Product] |
| [program_fee](/program_fee) | [Bus_Development_Program_Fee] | [Bus_Beta_Program_Fee] | [Bus_Production_Program_Fee] |
| [rejection](/rejection) | [Bus_Development_Rejection] | [Bus_Beta_Rejection] | [Bus_Production_Rejection] |
| [seller](/seller) | [Bus_Development_Seller] | [Bus_Beta_Seller] | [Bus_Production_Seller] |
| [seller_payment](/seller_payment) | [Bus_Development_Seller_Payment] | [Bus_Beta_Seller_Payment] | [Bus_Production_Seller_Payment] |
| [transaction](/transaction) | [Bus_Development_Transaction] | [Bus_Beta_Transaction] | [Bus_Production_Transaction] |

## Contribute

1. Install Docker Desktop for your OS
2. Fork the repo to your account and make changes in a new branch
3. Make sure validation passes with `docker-compose up`
3. Make sure you add the new type into .gitlab-ci.yml if it's not already there
4. Push your branch to Gitlab
5. Open a Pull Request

## Tagging (Publishes schema to S3)

After you merge the message_type updates into master you need to make two tag changes:
1. Point message_type.v1 tag to that master merge commit
2. Point message_type.v1.x tag to that master merge commit

## Help

This document is intended to cover both current and anticipated use cases - if you
don't find what you're looking for, please consult with leadership team member:

* @cakirke
* @daadiutori
* @dcharling
* @ebiven
* @jpulgar
* @skaragianis

[Enterprise Message Overview]: https://scm.multiservice.com/gitlab/architecture/enterprise-messaging/blob/master/README.md
[`"format": "date-time"` JSON-schema validation]: http://json-schema.org/latest/json-schema-validation.html#rfc.section.7.3.1
[E.164 Standard]: https://www.twilio.com/docs/glossary/what-e164
[jsonschema.net]: https://www.jsonschema.net/#
[Bus_Development_Accounts_Payable]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Accounts_Payable
[Bus_Development_Accounts_Receivable]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Accounts_Receivable
[Bus_Development_Adjustment]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Adjustment
[Bus_Development_Buyer]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Buyer
[Bus_Development_Buyer_Application]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Buyer_Application
[Bus_Development_Buyer_Card]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Buyer_Card
[Bus_Development_Buyer_Credit_Increase]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Buyer_Credit_Increase
[Bus_Development_Buyer_Payment]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Buyer_Payment
[Bus_Development_Contact]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Contact
[Bus_Development_Dispute]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Dispute
[Bus_Development_Keying]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Keying
[Bus_Development_Product]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Product
[Bus_Development_Program_Fee]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Program_Fee
[Bus_Development_Rejection]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Rejection
[Bus_Development_Seller]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Seller
[Bus_Development_Seller_Payment]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Seller_Payment
[Bus_Development_Transaction]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Development_Transaction
[Bus_Beta_Accounts_Payable]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Accounts_Payable
[Bus_Beta_Accounts_Receivable]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Accounts_Receivable
[Bus_Beta_Adjustment]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Adjustment
[Bus_Beta_Buyer]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Buyer
[Bus_Beta_Buyer_Application]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Buyer_Application
[Bus_Beta_Buyer_Card]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Buyer_Card
[Bus_Beta_Buyer_Credit_Increase]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Buyer_Credit_Increase
[Bus_Beta_Buyer_Payment]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Buyer_Payment
[Bus_Beta_Contact]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Contact
[Bus_Beta_Dispute]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Dispute
[Bus_Beta_Keying]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Keying
[Bus_Beta_Product]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Product
[Bus_Beta_Program_Fee]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Program_Fee
[Bus_Beta_Rejection]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Rejection
[Bus_Beta_Seller]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Seller
[Bus_Beta_Seller_Payment]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Seller_Payment
[Bus_Beta_Transaction]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Beta_Transaction
[Bus_Production_Accounts_Payable]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Accounts_Payable
[Bus_Production_Accounts_Receivable]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Accounts_Receivable
[Bus_Production_Adjustment]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Adjustment
[Bus_Production_Buyer]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Buyer
[Bus_Production_Buyer_Application]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Buyer_Application
[Bus_Production_Buyer_Card]:https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Buyer_Card
[Bus_Production_Buyer_Credit_Increase]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Buyer_Credit_Increase
[Bus_Production_Buyer_Payment]:https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Buyer_Payment
[Bus_Production_Contact]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Contact
[Bus_Production_Dispute]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Dispute
[Bus_Production_Keying]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Keying
[Bus_Production_Product]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Product
[Bus_Production_Program_Fee]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Program_Fee
[Bus_Production_Rejection]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Rejection
[Bus_Production_Seller]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Seller
[Bus_Production_Seller_Payment]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Seller_Payment
[Bus_Production_Transaction]: https://console.aws.amazon.com/sns/v3/home?region=us-east-1#/topic/arn:aws:sns:us-east-1:434875166128:Bus_Production_Transaction
