# Contact Message Payload Definition

This message handles disputes for the enterprise. Each message should contain data for a single contact. Projects will need to work with the Service Cloud team in order to get the needed information for setting up the internal_contact_id and internal_id

Required Fields
* client_id
* client_id_legacy
* contact_type
* internal_contact_id
* internal_id
* program_id

JSON Name | Comment
--- | ---
account_id | Buyer or Seller ID that the contact is associated with.
account_name | Buyer or Seller name the contact is associated with.
account_type | Type of account, Buyer/Seller, that the contact is associated with.
address.city | Contacts city.
address.country | Description of contacts country.
address.post_code | Contacts postal code.
address.state | Contacts state code.
address.street | Concatenated address1, address2, etc lines.
client_id | For GUESS projects this is analogous to the Network. So for 'PACCAR - Mexico' they will submit 'PM'. The network/client Cummins would submit 'CU'. For Australia projects, this will be the ID for the client, e.g., 74243728-8819-4cc0-9585-774e2ab1e7fc as the ID for Apruve. This value cannot change and must be unique.
client_id_legacy | For ALL projects this will be the 2-letter Network Identifier. So 'PACCAR - Australia' would submit 'PA', PACCAR would submit 'PP', Cummins would submit 'CU'.
contact_id | Buyer or Seller contact ID, if project does not have full contact support this will be a unique key based on where the contact data is pulled from. This unique key will have period (.) separators between the fields needed to make the value unique.  Ex: 01234.02.89 if three fields are needed or 00012345.06 if two fields are needed.
contact_role | Description of the contact role if the project supports it.
contact_status | Allows systems notify of a deactivated or deleted contact. Values will be Active, Inactive or Deleted. 
contact_type | Description of the contact type.
email_address | Email address associated with the contact.
email_notification.dunning | Are dunning notifications sent to this contact (true/false)
internal_contact_id | Constructed ID field used by Service Cloud to uniquely identify the Contact. Format of 3 character Project/Network character code - cont(A)ct - contact_id. See notes on contact_id for format info. Ex PAC-A-00000001234 or NAV-A-01234.89.05
internal_id | Constructed ID field used by Service Cloud to uniquely identify the Buyer or Seller. Format of 3 character Project/Network character code - single character C(Buyer) or M(Seller) - Buyer or Seller ID padded to 7 characters. Ex: PAC-C-0001234
name.first_name | Contacts first name, this will be NULL if the project only supports a single name field for contacts.
name.last_name | Contacts last name or the full contact name if the project only supports a single name field for contacts.
network_name | Network Name the contact is associated with.
phone.cell | The contacts cell number
phone.fax | The contacts fax number
phone.landline | The contacts landline number
program_id | The program under the client Apruve, e.g., Apruve.
user_type | Description of the user type if the project supports it.
