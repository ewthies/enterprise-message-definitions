# Buyer Card Message Payload Definition
This message handles Buyer Card for the enterprise. Each message should contain data for a single card.

Required Fields
* client_id
* client_id_legacy
* program_id
* buyer_id
* buyer_card_id

JSON Name | Comment
--- | ---
client_id | For GUESS projects this is analogous to the Network.
client_id_legacy | For ALL projects this will be the 2-letter Network Identifier.
program_id | The program under the client Apruve, e.g., Apruve.
buyer_id | Customer ID assigned by the system. Column Name: Customer ID
buyer_name | Merchant Provided Client Contact Name. Column Name: Client Contact Name
card_identifier | (May be masked.) The card identifier provided by the merchant. This may be null if the transaction is at an account level. Account level transactions are generated by the processing system, e.g., volume rebates etc... Column Name: Card Number
card_status | Cards current status. Column Name: Status
buyer_card_id | Customers Account Number in the merchant external system.  This is tied to a card. Column Name: Customer Account Number
owning_rep_email | Merchant Provided Sales Rep email address. Column Name: Sales Rep Email Address
owning_rep_name | Merchant Provided Sales Rep Name. Column Name: Sales Rep Name
team_indicator | Merchant Provided Sales Rep Team Indicator. Column Name: Sales Rep Team Indicator
territory | Merchant Provided Sales Rep Territory. Column Name: Sales Rep Territory
