# Seller Message Payload Definition

Required Fields
* client_id
* client_id_legacy
* program_id
* seller_id

JSON Name | Comment
--- | ---
accepted\_cards | Card types that the seller accepts Column Name: Accepted Cards
ap\_owner | Accounts Payable Owner - Who's bank account are payments going through for this transaction and who is responsible for exposure. Column Name: AP Owner
client\_division | Client-privided business division.
client\_id | The GUID for the client, e.g., Apruve is the client and it's the GUID for Apruve. 
client\_id\_legacy | Sellers network identifier. Column Name: Network ID
client\_identifier | The clients identifier for the seller. If blank then the project either does not have a client or the client did not provide their identifier for this seller. Column Name: Client Identifier
created\_at | Date the seller was initially created in the system. Column Name: Create Date
currencies.secondary | Client Currency - 3 Character currency code e.g. USD. Column Name: Client Currency
currencies.seller | Seller currency. Column Name: Currency
currencies.system | System Currency - 3 Character currency code e.g. USD. Column Name: System Currency
degrees\_latitude | The sellers latitude which is used for locating the seller. Column Name: Latitude
degrees\_longitude | The sellers longitude which is used for locating the seller. Column Name: Longitude
fee\_group | The fee group the seller is in. This determines the fee's that they are charged on every transaction processed. Column Name: Fee Group
first\_activation\_date | Date the seller was initially activated in the system. This is the date they would first be eligible to purchase. Column Name: Initial Activation Date
first\_transaction\_date | Sellers first date they made a transaction. Determined by transaction date. Column Name: First Transaction Date
icao | International Civil Aviation Organization for the seller if applicable. Column Name: ICAO
language | Seller language. Column Name: Language
last\_comment.author | User who entered last comment for seller. Column Name: Last Comment User
last\_comment.date | Date the last user comment was entered for seller. Column Name: Last Comment Date
last\_comment.text | Last user comment entered for seller. Column Name: Last Comment
last\_deactive\_date | Sellers last date they were deactivated in the system. Column Name: Deactivation Date
last\_deactive\_reason | Reason the seller was last deactivated. Column Name: Deactivation Reason
last\_pay\_out\_date | The date the the seller was last paid. Column Name: Last Payout Date
last\_transaction\_date | Sellers last date they made a transaction. Determined by transaction date. Column Name: Last Transaction Date
location\_name | Seller location Column Name: Location
name | The full, legal name of the seller. Column Name: Name
network\_name | Sellers network name. Column Name: Network Name
parent\_seller\_id | Parent seller ID for this seller. If blank then seller does not have a parent. Column Name: Parent Seller ID
parent\_seller\_name | Parent seller name for this seller. If blank then seller does not have a parent. Column Name: Parent Seller Name
payout\_method.method | Payout method system-specific/not strongly typed, e.g. "ACH", "Check".
payout\_method.last\_activation\_timestamp | Timestamp this method was last activated for this seller.
payout\_method.last\_deactivation\_timestamp | Timestamp this method was last deactivated for this seller.
physical\_contact.city | Seller physical contact address city. Column Name: Physical City
physical\_contact.contact | Seller physical contact. Column Name: Physical Contact
physical\_contact.country | Customers physical address country. Column Name: Physical Country
physical\_contact.email\_address | Seller physical contact email address. Column Name: Physical Email Address
physical\_contact.phone | Seller physical contact phone number. Column Name: Physical Phone Number
physical\_contact.post\_code | Seller physical contact address postal code. Column Name: Physical Postal Code
physical\_contact.state | Seller physical contact address state. Column Name: Physical State
physical\_contact.street | Seller physical contact address street. This can be made up of multiple address lines. Column Name: Physical Street
program\_id | The program under the client Apruve, e.g., Apruve.
receive\_physical\_mailing | Is the seller getting physical copies of the reports mailed to them. Column Name: Receive Physical Mailing
remittance\_contact.city | Seller remittance contact address city. Column Name: Remittance City
remittance\_contact.contact | Seller remittance contact. Column Name: Remittance Contact
remittance\_contact.country | Customers remittance address country. Column Name: Remittance Country
remittance\_contact.email\_address | Seller remittance contact email address. Column Name: Remittance Email Address
remittance\_contact.phone | Seller remittance contact phone number. Column Name: Remittance Phone Number
remittance\_contact.post\_code | Seller remittance contact address postal code. Column Name: Remittance Postal Code
remittance\_contact.state | Seller remittance contact address state. Column Name: Remittance State
remittance\_contact.street | Seller remittance contact address street. This can be made up of multiple address lines. Column Name: Remittance Street
seller\_id | Seller ID assigned by the system. Column Name: ID
seller\_type | The sellers w9 seller type. Column Name: Type
status | Sellers currenty account status. Column Name: Status
top\_seller\_id | Top level seller ID for this seller. Column Name: Top Level Seller ID
top\_seller\_name | Top level seller Name for this seller. Column Name: Top Level Seller Name
tax\_id | Tax identifiers associated with the seller. This is list of tax ids and not necessarily related to W9 information.
w9.name | Business name submitted on W9
w9.dba\_name | Doing Business As name submitted on W9
w9.tax\_account\_type | Tax account type submitted on W9. Will be the mnemonic identifier expected by W9/1099 service e.g. LimitedLiabilityCompany
w9.tax\_classification | Tax classification submitted on W9. Will be the mnemonic identifier expected by W9/1099 service e.g. DisregardedEntity
w9.address\_line\_1 | Address line one submitted on W9
w9.address\_line\_2 | Address line two submitted on W9
w9.address\_line\_3 | Address line three submitted on W9
w9.city | City submitted on W9
w9.state | State submitted on W9. Publisher should submit ISO code.
w9.post\_code | Postal code submitted on W9
w9.country | Country submitted on W9. Publisher should submit ISO code.
w9.tax\_id | Tax ID submitted on W9
w9.tax\_id\_type | The tax ID type submitted on W9. Will be the mnemonic identifier expected by W9/1099 service e.g., SSN, EIN, TIN, ITIN
w9.status | Seller 1099 participation status. Tells the W9/1099 service if this seller should have 1099 submitted. Active/Inactive
## Notes
