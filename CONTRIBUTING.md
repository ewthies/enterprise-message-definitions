# Contribution Guidelines

## Contributor Process
1. Fork the repository
2. Clone your fork of the repository
3. Create a branch in your fork of the repository
4. Make the proposed changes in your branch
5. Confirm tests pass locally in your branch
6. Commit changes
7. Push your branch
8. Create a merge request
9. Notify team members involved in the feature proposal to comment

### Helpful Git Forking Hints
#### To start, fork the repo.
1. Go to the gitlab web client and fork the repo. This will put a copy in your project namespace.
1. Clone your fork
    ```
    git clone git@scm.multiservice.com:dcharling/my-repo.git
    ```
1. Change into the cloned repo.
1. Add the original repo as a remote repo. Call it “upstream”.
    ```
    git remote add upstream git@scm.multiservice.com:architecture/enterprise-message-definitions.git
    ```
1. Fetch the original repo for changes.
    ```
    git fetch upstream
    ```

#### Before you make any changes or create any branches.
1. Make sure you are in the **master** branch of _your_ cloned repo.
1. Pull upstream changes.
    ```
    git pull upstream master
    ```
1. Push the upstream changes to your master.
    ```
    git push
    ```
1. Create branch.
    ```
    git checkout -b my-branch
    ```
1. Push the branch.
    ```
    git push --set-upstream origin my-branch
    ```

#### Before you do a Merge Request for your branch.
1. Get _your_ **master** up-to-date.
    ```
    git pull upstream master
    git push
    ```
1. Checkout your branch
1. Rebase the changes in Master into your branch
    ```
    git rebase master
    ```
1. Pull the changes which will generate  commit.
    ```
    git pull
    ```
1. Push the changes to the remote master
    ```
    git push
    ```

Your local branches won’t disappear on their own. Sometimes I delete from the web UI, but that deletion won’t delete things locally.

To list the branches
```
git branch -l
```

To delete a given branch
```
git branch -d branch-name-to-delete
```

### Helpful Docker Hints
1. For the first time, or any time Docker configs are updated, you need to run `docker-compose up --build`.
2. For subsequent events, you need to run `docker-compose up`.


## Maintainer Process
1. Facilitate discussion process if there are any participants from (9) above
2. Review merge request for all tests passing
3. Review merge request for descriptive/generic field names
4. Invite additional comment on the merge request where needed
5. Accept and merge where appropriate

