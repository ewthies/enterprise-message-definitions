# Rejection Message Payload Definition
This message handles Rejections for the enterprise. Only transactions that have been kicked out are included. There will be a message for each error. Messages for non-fatal errors should be sent ONLY if there was a fatal error on the transaction. If there are NO fatal errors then do NOT send a message. In the end, we only care about fatal errors.

Required Fields
* client_id
* client_id_legacy
* error_code
* program_id
* trx_id

JSON Name | Comment
--- | ---
batch_entry_mode | Identifies how batch entered the system e.g. keyed, settlement gateway etc. Column Name: Batch Entry Mode
batch_id | ID of the batch the trx is associated with, if there is an associated batch. Column Name: Batch ID
client_id | For GUESS projects this is analogous to the Network. So for 'PACCAR - Mexico' they will submit 'PM'. The network/client Cummins would submit 'CU'. For Australia projects, this will be the ID for the client, e.g., 74243728-8819-4cc0-9585-774e2ab1e7fc as the ID for Apruve. This value cannot change and must be unique.
client_id_legacy | For ALL projects this will be the 2-letter Network Identifier. So 'PACCAR - Australia' would submit 'PA', PACCAR would submit 'PP', Cummins would submit 'CU'.
error_code | Error Code that was generated returned for the error Column Name: Error Code
error_code_text | Text describing what the error was on the item Column Name: Error Text
error_level | Level of the error, can be Batch, Transaction or Detail. Column Name: Error Level
error_type | Is the error fatal or non-fatal Column Name: Error Type(Fatal/Non-Fatal)
invoice_date | The date/time for this transaction. This will normally be in the time zone of the merchant unless it is an internally generated transaction, those will be in system timezone. Column Name: Invoice Date
network_name | Business Unit Network Name. Column Name: Network Name
po_number | Purchase Order Number, this is used by some systems to enforce PO purchase policies. Column Name: PO Number
processing_date | Processing date. This will be the day this processing run is for. Column Name: Processing Date
program_id | The program under the client Apruve, e.g., Apruve.
purchasing_buyer_client_id | Customer client identifier Column Name: Purchasing Customer Client Identifier
purchasing_buyer_id | Customer client identifier Column Name: Purchasing Customer Identifer
purchasing_buyer_name | The full, legal name of the customer. Column Name: Purchasing Customer Name
purchasing_seller_client_id | Filled in with the merchant client identifer that delivered the batch of transactions. This could be assigned based on the cm_id from the batch or assigned based on the third party processor that sent us the batch/transactions.  Not all transactions (e.g. internally generated) will be associated with a Batch. Column Name: Purchasing Merchant Client Identifer
purchasing_seller_id | Filled in with the merchant business unit that delivered the batch of transactions. This could be assigned based on the cm_id from the batch or assigned based on the third party processor that sent us the batch/transactions.  Not all transactions (e.g. internally generated) will be associated with a Batch. Column Name: Purchasing Merchant ID
purchasing_seller_name | The full, legal name of the Merchant. Column Name: Purchasing Merchant Name
rejection_expiration | When a fatal error rejection will expire or has expired Column Name: Rejection Expiration
rejection_status | Status of the fatal rejection Column Name: Rejection Status
release_user | User who released the error Column Name: Release User
released | Has the rejection been released Column Name: Released
second_notice | When a second notice will be sent or was sent for a fatal rejection Column Name: Second Notice
seller_currency | Merchant Currency. Column Name: Merchant Currency
seller_submitted_total | Submitted total in merchant currency (This will include payable and pass through amounts) (NOTE: Will include any pre-processing amounts e.g. Avalara Taxes). Column Name: Submitted Total (Merchant Currency)
transaction_reference | Transaction reference assigned by merchant and usually the one printed on the customer's ticket. Column Name: Invoice Number
trx_detail_id | ID of the detail if there is an associated detail. Column Name: Transaction Detail ID
trx_id | ID of the trx is associated with, if there is an associated transaction. Column Name: Transaction ID

# Notes

