# Adjustment Message Payload Definition
This message handles Adjustments for the Enterprise.

Required Fields
* adjustment_id
* adjustment_source
* bu_adjustment_id
* client_id
* clienty_id_legacy
* program_id


JSON Name | Comment
--- | ---
adjustment_batch_run_id | For projects that support running adjustment requests in batch this will be the batch run ID that processed the adjustment request and subsequent adjustment. Column Name: Adjustment Batch ID
adjustment_entered_at | This is the date/time the adjustment was processed. Column Name: Adjustment Timestamp
adjustment_id | This is the system identifier for the adjustment. Column Name: Adjustment ID
adjustment_reason | Reason for performing the adjustment Column Name: Adjustment Reason
adjustment_source | Each row of data either refers to the Customer side of an adjustment or the Merchant side of an adjustment. This defines which side the row is referring to. Column Name: Adjustment (Customer/Merchant/System)
adjustment_user | This is the name of the user that performed the adjustment. Column Name: Adjustment User
ap_owner | Accounts Payable Owner - Who's bank account merchant payables are going through. Column Name: AP Owner
ap_service_fee_total.secondary_amount | Merchant accounts payable service fee portion of the total adjustment amount in client currency. Left blank for account level adjustments, customer adjustment records and projects with no client. Column Name: AP Service Fee Total (Client Currency)
ap_service_fee_total.seller_amount | Merchant accounts payable service fee portion of the total adjustment amount in merchant currency. Left blank for account level adjustments and customer adjustment records. Column Name: AP Service Fee Total (Merchant Currency)
ap_service_fee_total.system_amount | Merchant accounts payable service fee portion of the total adjustment amount in system currency. Left blank for account level adjustments and customer adjustment records. Column Name: AP Service Fee total (System Currency)
ap_total.secondary_amount | Merchant accounts payable adjustment total in client currency. Left blank for customer adjustment records and projects with no client. Column Name: AP Total (Client Currency)
ap_total.seller_amount | Merchant accounts payable adjustment total in merchant currency. Left blank for customer adjustment records. Column Name: AP Total (Merchant Currency)
ap_total.system_amount | Merchant accounts payable adjustment total in system currency. Left blank for customer adjustment records. Column Name: AP Total (System Currency)
approval_code | For transactional adjustments this will be the approval code of the transaction being adjusted. For account level and payment level adjustments this will be left blank. Column Name: Approval Code
ar_owner | Accounts Receivable Owner - Who's bank account payments are going through for this transaction and who is responsible for exposure if not in recourse. Column Name: AR Owner
ar_service_fee_total.buyer_amount | Customer accounts receivable service fee portion of the total adjustment amount in customer currency. Left blank for account level adjustments. This value will only be populated if we bill the merchant service fee to a customer. Column Name: AR Service Fee Total (Customer Currency)
ar_service_fee_total.secondary_amount | Customer accounts receivable service fee portion of the total adjustment amount in client currency. Left blank for account level adjustments and projects with no client. This value will only be populated if we bill the merchant service fee to a customer. Column Name: AR Service Fee Total (Client Currency)
ar_service_fee_total.system_amount | Customer accounts receivable service fee portion of the total adjustment amount in system currency. Left blank for account level adjustments. This value will only be populated if we bill the merchant service fee to a customer. Column Name: AR Service Fee Total (System Currency)
ar_total.buyer_amount | Customer accounts receivable adjustment total in customer currency. Left blank for merchant adjustment records. Column Name: AR Total (Customer Currency)
ar_total.secondary_amount | Customer accounts receivable adjustment total in client currency. Left blank for merchant adjustment records and projects with no client. Column Name: AR Total (Client Currency)
ar_total.system_amount | Customer accounts receivable adjustment total in system currency. Left blank for merchant adjustment records. Column Name: AR Total (System Currency)
backed_out | Has this adjustment been back out of Y(es) or N(o)? Column Name: Adjustment Backed Out? (Y/N)
backout_adjustment_id | The adjustment identifier of the adjustment that backed out of this adjustment. If the Adjustment ID and Backout Adjustment ID are the same then this is a backout adjustment. Column Name: Backout Adjustment ID
bill_date | The billing date the adjustment was billed to the customer on if a customer adjustment. Column Name: Bill Date
book_bill_history_id | The Billing Run ID that the adjustment was booked to the AR on if a customer adjustment. Column Name: Bill Book ID
bu_adjustment_id | The Business Unit Adjustment ID under a given Adjust ID. If we split bill or split reimburse then we may have multiple customer or merchant records for the same Adjust ID. This should be NULL if you do not support that level of adjustment. Column Name: Business Adjustment Unit ID
buyer_adjustment_total.buyer_amount | Customer adjustment total in customer currency. Left blank for merchant adjustment records. Column Name: Customer Adjustment Total (Customer Currency)
buyer_adjustment_total.secondary_amount | Customer adjustment total in client currency. Left blank for merchant adjustment records and projects with no client. Column Name: Customer Adjustment Total (Client Currency)
buyer_adjustment_total.system_amount | Customer adjustment total in system currency. Left blank for merchant adjustment records. Column Name: Customer Adjustment Total (System Currency)
buyer_adjustment_type | Customer adjustment type ID. Left blank for rows with adjustment source of Merchant. Column Name: Customer Adjustment Type ID
buyer_adjustment_type_text | Customer adjustment type. Left blank for rows with adjustment source of Merchant. Column Name: Customer Adjustment Type
buyer_id | The Customer ID the adjustment was done against if a customer adjustment. Column Name: Customer ID
buyer_name | Customer name the adjustment was done against. Left blank for merchant adjustments. Column Name: Customer Name
buyer_pass_through | Did the adjustment have no affect the accounts receivable Y(es) or N(o). Left blank for merchant adjustment records. Column Name: Customer Pass Through? (Y/N)
card_identifier | For transactional adjustments this is the card identifier of the transaction being adjusted. May be masked. Column Name: Card Number
card_type | For transactional adjustments this will be the card type of the transaction being adjusted. For account level and payment�vel adjustments this will be left blank. Column Name: Card Type
client_ap_total.secondary_amount | For transactional adjustments this is the client billable amount of the accounts payable total of transaction being adjusted in Client currency. Column Name: Client AP Total (Client Currency)
client_ap_total.seller_amount | For transactional adjustments this is the client billable amount of the accounts payable total of transaction being adjusted in Merchant currency. Column Name: Client AP Total (Merchant Currency)
client_ap_total.system_amount | For transactional adjustments this is the client billable amount of the accounts payable total of transaction being adjusted in System currency. Column Name: Client AP Total (System Currency)
client_ar_total.buyer_amount | For transactional adjustments this is the client billable amount of the accounts receivable total of transaction being adjusted in Customer currency. Column Name: Client AR Total (Customer Currency)
client_ar_total.secondary_amount | For transactional adjustments this is the client billable amount of the accounts receivable total of transaction being adjusted in Client currency. Column Name: Client AR Total (Client Currency)
client_ar_total.system_amount | For transactional adjustments this is the client billable amount of the accounts receivable total of transaction being adjusted in System currency. Column Name: Client AR Total (System Currency)
client_id | For GUESS projects this is analogous to the Network. So for 'PACCAR - Mexico' they will submit 'PM'. The network/client Cummins would submit 'CU'. For Australia projects, this will be the ID for the client, e.g., 74243728-8819-4cc0-9585-774e2ab1e7fc as the ID for Apruve. This value cannot change and must be unique.
client_id_legacy | For ALL projects this will be the 2-letter Network Identifier. So 'PACCAR - Australia' would submit 'PA', PACCAR would submit 'PP', Cummins would submit 'CU'.
currencies.buyer | Customer currency Column Name: Customer Currency
currencies.secondary | Currency that the client totals are in. Column Name: Client Currency
currencies.seller | Merchant currency Column Name: Merchant Currency
currencies.system | System currency. Column Name: System Currency
in_dispute | Is the transaction being adjusted in dispute? Y(es) or N(o)   Left blank for non transactional adjustments. Column Name: In Dispute? (Y/N)
in_recourse | Is the account in recourse? Y(es) or N(o)  Meaning a third party is now responsible for the exposure for the transaction (usually the merchant or the client). Left blank for merchant adjustment records and when project does not support recourse. Column Name: In Recourse? (Y/N)
invoice_date | The transaction date/time of the transaction being adjusted. Left blank for non transactional adjustments. Column Name: Invoice Date
network_name | Network name the adjustment was done under. Column Name: Network
pay_out_id | Internally assigned payment ID when projected to pay out during processing. Left blank for customer adjustment records. Column Name: Pay ID
po_number | Purchase order number of transaction. Left blank for non transactional adjustments. Column Name: PO Number
processing_history_id | Transaction processing run ID that pulled the adjustment in for processing. Column Name: Processing Run ID
processing_date | Transaction processing date that the adjustment was pulled into. Column Name: Processing Date
processor_fee.seller_amount | Amount charged by third party processor in merchant currency. Left blank when not applicable or available. Column Name: Processor Fee (Merchant Currency)
processor_fee.system_amount | The processor fee originally charged for the transaction being adjusted Column Name: Processor Fee (System Currency)
program_fee_id | Max program fee ID applied to details of transaction being adjusted. If program_fee_id_count > 1 then this will not be representing all program fee IDs that were applied. Left blank for non transactional adjustments. Column Name: Program Fee ID
program_fee_id_count | Count of distinct program fee IDs applied to the transaction being adjusted. If this is > 1 then we are applying different program fees across details for that transaction. Left blank for non transactional adjustments. Column Name: Program Fee ID Count
program_fee.secondary_amount | The program fee in client currency of the transaction being adjusted. Left blank for non transactional adjustments and projects with no client. Column Name: Program Fee (Client Currency)
program_fee.system_amount | The program fee in system currency of the transaction being adjusted. Left blank for non transactional adjustments. Column Name: Program Fee (System Currency)
program\_id | The program under the client Apruve, e.g., Apruve.
projected_pay_out_date | The projected pay out date for this adjustment. This may be different than the actual pay out date due to the holding of payment. Left blank for customer adjustment records. Column Name: Max Projected Pay Out Date
purchasing_buyer_id | Customer ID that the card belongs to at time of transaction based on transaction date or the customer the transaction belongs to if the transaction is not card based. If the transaction is a Bank Card purchasing card, the associated customer will populate this column. This can be null if it is a third party card where we do not know the customer. Left blank for non transactional adjustments. Column Name: Purchasing Customer ID
purchasing_buyer_name | The full, legal name of the customer for the transaction being adjusted. Left blank for non transactional adjustments. Column Name: Purchasing Customer Name
purchasing_seller_id | Filled in with the merchant business unit ID that delivered the batch of transactions. This could be assigned based on the cm_id from the batch or assigned based on the third party processor that sent us the batch/transactions. Not all transactions (e.g. internally generated) will be associated with a Batch. Left blank for non transactional adjustments. Column Name: Purchasing Merchant ID
purchasing_seller_name | The full, legal name of the Merchant for the transaction being adjusted. Left blank for non transactional adjustments. Column Name: Purchasing Merchant Name
request_id | If the adjustment was submitted via adjustment request functionality this will be the request identifier. Left blank for adjustments that did not go through a request process. Column Name: Adjustment Request ID
seller_adjustment_total.secondary_amount | Merchant adjustment total in client currency. Left blank for customer adjustment records and projects with no client. Column Name: Merchant Adjustment Total (Client Currency)
seller_adjustment_total.seller_amount | Merchant adjustment total in merchant currency. Column Name: Merchant Adjustment Total (Merchant Currency)
seller_adjustment_total.system_amount | Merchant adjustment total in system currency. Left blank for customer adjustment records. Column Name: Merchant Adjustment Total (System Currency)
seller_adjustment_type | Adjustment type ID for the adjustment type that was performed on the merchants account. Left blank for customer adjustment records. Column Name: Merchant Adjustment Type ID
seller_adjustment_type_text | Adjustment type that was performed on the merchants account. Left blank for customer adjustment records. Column Name: Merchant Adjustment Type
seller_id | The Merchant ID the adjustment was done against if a merchant adjustment. Column Name: Merchant ID
seller_name | Merchant name the adjustment was done against. Left blank for customer adjustments. Column Name: Merchant Name
seller_pass_through | Did the adjustment have no affect on the accounts payable Y(es) or N(o). Left blank for customer adjustment records. Column Name: Merchant Pass Through? (Y/N)
seller_pay_status | Merchant payment/reimbursement status of adjustment. If a seller/merchant side adjustment results in a payout/reimbursement to the merchant, this field contains the status of that payment. In theory this could be a negative amount (a merchant chargeback) and the payment could be held until it goes positive if we are unable to direct debit the merchant (hence all the Held statuses). Example Values Held, Paid, Pending, Pending to be paid.  Column Name: Merchant Payment Status
transaction_id | The transaction ID of the transaction being adjusted. This is assigned by the processing system. Left blank for non transactional adjustments. Column Name: Transaction ID
transaction_reference | The transaction reference of the transaction being adjusted. Usually the identifier that would print on a receipt. Left blank for non transactional adjustments. Column Name: Invoice Number
transaction_type | The transaction type of the transaction being adjusted. Left blank for non transactional adjustments. Column Name: Transaction Type
user_comment | Adjustment comment used to explain why the adjustment was done. Column Name: Adjustment User Comment


