# Product Message Payload Definition
This message handles the Product dataset for the enterprise. This is a monthly summation of Product data. Values are summed by the following dimensions:
1. month
1. client_id (network)
1. item\_id
1. item\_sku\_id
1. item\_sku
1. purchasing\_buyer\_id
1. purchasing\_seller\_id
1. seller_uom

This message should be sent once a month. There will be one message for each combination. For an example, the customer, "City of Anaheim", for the client, "Best Buy", happened to have purchased goods from ONLY one seller in the month. The summarized dataset would have five rows and would result in FIVE product messages. Here is an example. (Many of the columns have been removed for clarity.)

Processing_month | Item_id | Item_description | Item_SKU | Item\_SKU\_description | Item_category | Buyer | Seller
--- | ---: | --- | --- | --- | --- | --- | ---
01-May-19 | 3 | Sales Tax | NULL | Sales Tax | Tax | CITY OF ANAHEIM | BBF8
01-May-19 | 32000000 | Product | BB00000001 | Product | Product | CITY OF ANAHEIM | BBF8
01-May-19 | 32000000 | Product | BB20997985 | Product | Product | CITY OF ANAHEIM | BBF8
01-May-19 | 32000000 | Product | BB20999070 | Product | Product | CITY OF ANAHEIM | BBF8
01-May-19 | 32000002 | Shipping | NULL | Shipping | Shipping | CITY OF ANAHEIM | BBF8


Required Fields
* processing_month
* client_id
* item_id
* item\_sku\_id
* program_id
* purchasing\_buyer\_id
* purchasing\_seller\_id

JSON Name | Comment
--- | ---
ap\_adjusted_total.secondary_amount | Total merchant payable price adjusted total for purchasing merchant, purchasing customer and product across processing month in client currency.
ap\_adjusted_total.seller_amount | Total merchant payable price adjusted total for purchasing merchant, purchasing customer and product across processing month in merchant currency.
ap\_adjusted_total.system_amount | Total merchant payable price adjusted total for purchasing merchant, purchasing customer and product across processing month in system currency.
ap\_service_fee_total.secondary_amount | Total merchant payable service fee total for purchasing merchant, purchasing customer and product across processing month in client currency.
ap\_service_fee_total.seller_amount | Total merchant payable service fee total for purchasing merchant, purchasing customer and product across processing month in merchant currency.
ap\_service_fee_total.system_amount | Total merchant payable service fee total for purchasing merchant, purchasing customer and product across processing month in system currency.
ap\_total.secondary_amount | Total submitted amount that is payable to merchant in client currency.
ap\_total.seller_amount | Total submitted amount that is payable to merchant in merchant currency.
ap\_total.system_amount | Total submitted amount that is payable to merchant in system currency.
ar\_adjusted_total.buyer_amount | Total customer receivable price adjusted total for purchasing merchant, purchasing customer and product across processing month in customer currency.
ar\_adjusted_total.secondary_amount | Total customer receivable price adjusted total for purchasing merchant, purchasing customer and product across processing month in client currency.
ar\_adjusted_total.system_amount | Total customer receivable price adjusted total for purchasing merchant, purchasing customer and product across processing month in system currency.
ar\_service_fee_total.buyer_amount | Total customer receivable service fee total for purchasing merchant, purchasing customer and product across processing month in customer currency.
ar\_service_fee_total.secondary_amount | Total customer receivable service fee total for purchasing merchant, purchasing customer and product across processing month in client currency.
ar\_service_fee_total.system_amount | Total customer receivable service fee total for purchasing merchant, purchasing customer and product across processing month in system currency.
ar\_total.buyer_amount | Total amount billed to customer in customer currency.
ar\_total.secondary_amount | Total amount billed to customer in client currency.
ar\_total.system_amount | Total amount billed to customer in system currency.
avg\_ap_unit_price.secondary_amount | Average merchant payable unit price for purchasing merchant, purchasing customer and product across processing month in client currency.
avg\_ap_unit_price.seller_amount | Average merchant payable unit price for purchasing merchant, purchasing customer and product across processing month in merchant currency.
avg\_ap_unit_price.system_amount | Average merchant payable unit price for purchasing merchant, purchasing customer and product across processing month in system currency.
avg\_ar_unit_price.buyer_amount | Average customer unit price for purchasing merchant, purchasing customer and product across processing month in customer currency.
avg\_ar_unit_price.secondary_amount | Average customer unit price for purchasing merchant, purchasing customer and product across processing month in client currency.
avg\_ar_unit_price.system_amount | Average customer unit price for purchasing merchant, purchasing customer and product across processing month in system currency.
avg\_submitted_unit_price.secondary_amount | Average merchant submitted unit price for purchasing merchant, purchasing customer and product across processing month in client currency.
avg\_submitted_unit_price.seller_amount | Average merchant submitted unit price for purchasing merchant, purchasing customer and product across processing month in merchant currency.
avg\_submitted_unit_price.system_amount | Average merchant submitted unit price for purchasing merchant, purchasing customer and product across processing month in system currency.
client_id | For GUESS projects this is analogous to the Network. So for 'PACCAR - Mexico' they will submit 'PM'. The network/client Cummins would submit 'CU'. For Australia projects, this will be the ID for the client, e.g., 74243728-8819-4cc0-9585-774e2ab1e7fc as the ID for Apruve. This value cannot change and must be unique.
client\_id_legacy | For ALL projects this will be the 2-letter Network Identifier. So 'PACCAR - Australia' would submit 'PA', PACCAR would submit 'PP', Cummins would submit 'CU'.
currencies.buyer | Purchasing Customer Currency - 3 Character currency code e.g. USD.
currencies.secondary | Client Currency - 3 Character currency code e.g. USD.
currencies.seller | Currency the Merchant transacts in.
currencies.system | System Currency - 3 Character currency code e.g. USD.
item_category | Category that the item or product falls under. If the item does not fall into our standard list of categories then we will assign it to Unknown.
item_description | Item (Product) Description associated to the Item (Product) ID.
item_id | Item (Product) ID - Identifier for the item or product submitted by the merchant or generated by the system.
item_sku | Item (Product) SKU - The stock keeping unit or product code provided by the merchant. This can be used to look up a specific item or product being sold in a client cataglog.
item_sku_description | SKU Description associated with Item (Product) SKU ID.
item_sku_id | Item (Product) SKU ID - Identifier for the specific item or product based on the SKU or part number. If we do not identify the SKU provided then this will be the same as the Item (Product) ID.
max\_ap_unit_price.secondary_amount | Maximum merchant payable unit price for purchasing merchant, purchasing customer and product across processing month in client currency.
max\_ap_unit_price.seller_amount | Maximum merchant payable unit price for purchasing merchant, purchasing customer and product across processing month in merchant currency.
max\_ap_unit_price.system_amount | Maximum merchant payable unit price for purchasing merchant, purchasing customer and product across processing month in system currency.
max\_ar_unit_price.buyer_amount | Maximum customer unit price for purchasing merchant, purchasing customer and product across processing month in customer currency.
max\_ar_unit_price.secondary_amount | Maximum customer unit price for purchasing merchant, purchasing customer and product across processing month in client currency.
max\_ar_unit_price.system_amount | Maximum customer unit price for purchasing merchant, purchasing customer and product across processing month in system currency.
max\_submitted_unit_price.secondary_amount | Maximum merchant submitted unit price for purchasing merchant, purchasing customer and product across processing month in client currency.
max\_submitted_unit_price.seller_amount | Maximum merchant submitted unit price for purchasing merchant, purchasing customer and product across processing month in merchant currency.
max\_submitted_unit_price.system_amount | Maximum merchant submitted unit price for purchasing merchant, purchasing customer and product across processing month in system currency.
min\_ap_unit_price.secondary_amount | Minimum merchant payable unit price for purchasing merchant, purchasing customer and product across processing month in client currency.
min\_ap_unit_price.seller_amount | Minimum merchant payable unit price for purchasing merchant, purchasing customer and product across processing month in merchant currency.
min\_ap_unit_price.system_amount | Minimum merchant payable unit price for purchasing merchant, purchasing customer and product across processing month in system currency.
min\_ar_unit_price.buyer_amount | Minimum customer unit price for purchasing merchant, purchasing customer and product across processing month in customer currency.
min\_ar_unit_price.secondary_amount | Minimum customer unit price for purchasing merchant, purchasing customer and product across processing month in client currency.
min\_ar_unit_price.system_amount | Minimum customer unit price for purchasing merchant, purchasing customer and product across processing month in system currency.
min\_submitted_unit_price.secondary_amount | Minimum merchant submitted unit price for purchasing merchant, purchasing customer and product across processing month in client currency.
min\_submitted_unit_price.seller_amount | Minimum merchant submitted unit price for purchasing merchant, purchasing customer and product across processing month in merchant currency.
min\_submitted_unit_price.system_amount | Minimum merchant submitted unit price for purchasing merchant, purchasing customer and product across processing month in system currency.
network_name | Business Unit Network Name.
processing_month | Displays as the first day of the month for the processing month and year of transaction used to roll up totals by. Stored as a date to make use in BI tools easier.
program_id | For GUESS projects, this will be the Schema name, e.g., DAF. For Australia projects, this will be the ID for the program, e.g, e2c67ae4-39d9-4220-95b2-d1335d83d5d7 for 4MD Premium. This value cannot change and must be unique. 
purchasing\_buyer_client_id | Client identifier for purchasing customer.
purchasing\_buyer_country | Purchasing customers billing address country.
purchasing\_buyer_id | Customer that the card belongs to at time of transaction based on transaction date or the customer the transaction belongs to if the transaction is not card based.  If the transaction is a Bank Card purchasing card, the associated customer will populate this column. This can be null if it is a third party card where we do not know the customer.
purchasing\_buyer_name | The full, legal name of the customer.
purchasing\_buyer_state | Purchasing customers billing address state.
purchasing\_seller_client_id | Client identifier for purchasing merchant.
purchasing\_seller_id | Filled in with the merchant business unit that delivered the batch of transactions. This could be assigned based on the cm_id from the batch or assigned based on the third party processor that sent us the batch/transactions.
purchasing\_seller_name | The full, legal name of the Purchasing Merchant.
purchasing\_seller_physical_country | Physical address country of purchasing merchant.
purchasing\_seller_physical_state | Physical address state of purchasing merchant.
purchasing\_seller_remittance_country | Remittance address country of purchasing merchant.
purchasing\_seller_remittance_state | Remittance address state of purchasing merchant.
submitted_total.buyer_amount | Total amount submitted by merchant for purchasing merchant, purchasing customer and product across processing month in customer currency.
submitted_total.secondary_amount | Total amount submitted by merchant for purchasing merchant, purchasing customer and product across processing month in client currency.
submitted_total.seller_amount | Total amount submitted by merchant for purchasing merchant, purchasing customer and product across processing month in merchant currency.
submitted_total.system_amount | Total amount submitted by merchant for purchasing merchant, purchasing customer and product across processing month in system currency.
total_quantity | Total quantity rolled up for purchasing merchant, purchasing customer, product and processing month.
transaction_count | Number of transactions that contained purchasing merchant, purchasing customer, product for processing month. NOTE: A single transaction can be included in more than one product rollup.
unit\_of_measure | Unit of measure the quantity is in

# Notes

