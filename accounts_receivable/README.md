# Accounts Receivable Message Payload Definition

JSON Name | Comment
--- | ---
ar_total.buyer_amount | Total accounts receivable in customer currency. This is the total unpaid transactions minus unapplied payments.  Column Name: AR Total (Customer Currency)
ar_total.secondary_amount | Total accounts receivable in client currency. This is the total unpaid transactions minus unapplied payments.  Column Name: AR Total (Client Currency)
ar_total.system_amount | Total accounts receivable in system currency. This is the total unpaid transactions minus unapplied payments.  Column Name: AR Total (System Currency)
auth_total.buyer_amount | Total pending authorizations in customer currency. Authorizations that have not expired and have not been matched to a processed transactions.  Column Name: Authorization Total (Customer Currency)
auth_total.secondary_amount | Total pending authorizations in client currency. Authorizations that have not expired and have not been matched to a processed transactions.  Column Name: Authorization Total (Client Currency)
auth_total.system_amount | Total pending authorizations in system currency. Authorizations that have not expired and have not been matched to a processed transactions.  Column Name: Authorization Total (System Currency)
bill_ar_owner | Accounts Receivable Owner - Who's bank account are payments going through for this transaction and who is responsible for exposure if not in recourse. Column Name: AR Owner
billing_buyer_id | The billing customer ID. Column Name: Billing Customer ID
billing_buyer.amount_credit | Credit line assigned to the billing customer. If they are drawing their credit of their parent then this will define the billing customers limit of the parents credit line. Column Name: Billing Customer Credit Line
billing_buyer.amount_exposure | The amount that has been used of their credit line. This is made up of active authorizations + transactions pending to be billed + unpaid (open) transactions. Column Name: Billing Customer Exposure
billing_buyer.available_credit | This is the billing customers credit line minus their exposure. Column Name: Available Credit
client_id | For GUESS projects this is analogous to the Network. So for 'PACCAR - Mexico' they will submit 'PM'. The network/client Cummins would submit 'CU'. For Australia projects, this will be the ID for the client, e.g., 74243728-8819-4cc0-9585-774e2ab1e7fc as the ID for Apruve. This value cannot change and must be unique.
client_id_legacy | For ALL projects this will be the 2-letter Network Identifier. So 'PACCAR - Australia' would submit 'PA', PACCAR would submit 'PP', Cummins would submit 'CU'.
client_identifier | The clients identifier for the customer. If blank then the project either does not have a client or the client did not provide their identifier for this customer. Column Name: Client Identifier
credit_aging_1_7.buyer_amount | Credit aging in customer currency. Total unpaid amount past due between 1 and 7 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 1-7 (Customer Currency)
credit_aging_1_7.secondary_amount | Credit aging in client currency. Total unpaid amount past due between 1 and 7 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 1-7 (Client Currency)
credit_aging_1_7.system_amount | Credit aging in system currency. Total unpaid amount past due between 1 and 7 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 1-7 (System Currency)
credit_aging_15_30.buyer_amount | Credit aging in customer currency. Total unpaid amount past due between 15 and 30 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 15-30 (Customer Currency)
credit_aging_15_30.secondary_amount | Credit aging in client currency. Total unpaid amount past due between 15 and 30 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 15-30 (Client Currency)
credit_aging_15_30.system_amount | Credit aging in system currency. Total unpaid amount past due between 15 and 30 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 15-30 (System Currency)
credit_aging_31_45.buyer_amount | Credit aging in customer currency. Total unpaid amount past due between 31 and 45 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 31-45 (Customer Currency)
credit_aging_31_45.secondary_amount | Credit aging in client currency. Total unpaid amount past due between 31 and 45 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 31-45 (Client Currency)
credit_aging_31_45.system_amount | Credit aging in system currency. Total unpaid amount past due between 31 and 45 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 31-45 (System Currency)
credit_aging_46_60.buyer_amount | Credit aging in customer currency. Total unpaid amount past due between 46 and 60 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 46-60 (Customer Currency)
credit_aging_46_60.secondary_amount | Credit aging in client currency. Total unpaid amount past due between 46 and 60 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 46-60 (Client Currency)
credit_aging_46_60.system_amount | Credit aging in system currency. Total unpaid amount past due between 46 and 60 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 46-60 (System Currency)
credit_aging_61_90.buyer_amount | Credit aging in customer currency. Total unpaid amount past due between 61 and 90 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 61-90 (Customer Currency)
credit_aging_61_90.secondary_amount | Credit aging in client currency. Total unpaid amount past due between 61 and 90 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 61-90 (Client Currency)
credit_aging_61_90.system_amount | Credit aging in system currency. Total unpaid amount past due between 61 and 90 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 61-90 (System Currency)
credit_aging_8_14.buyer_amount | Credit aging in customer currency. Total unpaid amount past due between 8 and 14 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 8-14 (Customer Currency)
credit_aging_8_14.secondary_amount | Credit aging in client currency. Total unpaid amount past due between 8 and 14 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 8-14 (Client Currency)
credit_aging_8_14.system_amount | Credit aging in system currency. Total unpaid amount past due between 8 and 14 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 8-14 (System Currency)
credit_aging_91_120.buyer_amount | Credit aging in customer currency. Total unpaid amount past due between 91 and 120 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 91-120 (Customer Currency)
credit_aging_91_120.secondary_amount | Credit aging in client currency. Total unpaid amount past due between 91 and 120 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 91-120 (Client Currency)
credit_aging_91_120.system_amount | Credit aging in system currency. Total unpaid amount past due between 91 and 120 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 91-120 (System Currency)
credit_aging_late_fee_total.buyer_amount | Total of late fees that are past due in customer currency. Column Name: Credit Extended AR Late Charge Total (Customer Currency)
credit_aging_late_fee_total.secondary_amount | Total of late fees that are past due in client currency. Column Name: Credit Extended AR Late Charge Total (Client Currency)
credit_aging_late_fee_total.sytem_amount | Total of late fees that are past due in system currency. Column Name: Credit Extended AR Late Charge Total (System Currency)
credit_aging_less_pay_total.buyer_amount | Total of transactions past due less unapplied payments in customer currency. Column Name: Net Delinquent (Customer Currency)
credit_aging_less_pay_total.seconary_amount | Total of transactions past due less unapplied payments in client currency. Column Name: Net Delinquent (Client Currency)
credit_aging_less_pay_total.system_amount | Total of transactions past due less unapplied payments in system currency. Column Name: Net Delinquent (System Currency)
credit_aging_over_120.buyer_amount | Credit aging in customer currency. Total unpaid amount past due over 120 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 121+ (Customer Currency)
credit_aging_over_120.secondary_amount | Credit aging in client currency. Total unpaid amount past due over 120 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 121+ (Client Currency)
credit_aging_over_120.system_amount | Credit aging in system currency. Total unpaid amount past due over 120 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 121+ (System Currency)
credit_aging_over_60.buyer_amount | Credit aging in customer currency. Total unpaid amount past due over 60 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 61+ (Customer Currency)
credit_aging_over_60.secondary_amount | Credit aging in client currency. Total unpaid amount past due over 60 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 61+ (Client Currency)
credit_aging_over_60.system_amount | Credit aging in system currency. Total unpaid amount past due over 60 days. Unapplied credits are not aged.  Column Name: Credit Extended AR Late 61+ (System Currency)
credit_buyer_id | The credit customers ID. This will be the same as the billing customer ID if getting their own credit.  Column Name: Credit Customer ID
credit_buyer.amount_credit | Credit line assigned to the credit customer for the billing customer. If they are using their own credit then this will be the same as the billing credit line. Column Name: Credit Customer Credit Limit
credit_buyer.amount_exposure | The amount that has been used of the credit customers credit line. This is made up of active authorizations + transactions pending to be billed + unpaid (open) transactions for all customers drawing off of their credit. Column Name: Credit Customer Exposure
credit_extended_ar_total.buyer_amount | Total transactions not past due in customer currency. Column Name: Credit Extended AR Current (Customer Currency)
credit_extended_ar_total.secondary_amount | Total transactions not past due in client currency. Column Name: Credit Extended AR Current (Client Currency)
credit_extended_ar_total.system_amount | Total transactions not past due in system currency. Column Name: Credit Extended AR Current (System Currency)
currencies.buyer | Billing customers currency. Column Name: Billing Customer Currency
currencies.secondary | Currency that the client totals are in. If blank then the project does not have a client. Column Name: Client Currency
currencies.system | System Currency Column Name: System Currency
current_days_to_pay | Billing customers current days to pay. This is calculated by taking the last four applied payments and averaging the number of days between the billing date and payment date.  Column Name: Current Days to Pay
disputed_total.buyer_amount | Total transactions that are marked in dispute in customer currency.  Column Name: Disputed Total (Customer Currency)
disputed_total.secondary_amount | Total transactions that are marked in dispute in client currency.  Column Name: Disputed Total (Client Currency)
disputed_total.system_amount | Total transactions that are marked in dispute in system currency.  Column Name: Disputed Total (System Currency)
fc_aging_0_14.buyer_amount | Financial Controls aging in customer currency. Unpaid transactions billed minus unapplied payments deposited in the last 14 days.  Column Name: FC AR Aging 0-14 (Customer Currency)
fc_aging_0_14.secondary_amount | Financial Controls aging in client currency. Unpaid transactions billed minus unapplied payments deposited in the last 14 days.  Column Name: FC AR Aging 0-14 (Client Currency)
fc_aging_0_14.system_amount | Financial Controls aging in system currency. Unpaid transactions billed minus unapplied payments deposited in the last 14 days.  Column Name: FC AR Aging 0-14 (System Currency)
fc_aging_15_28.buyer_amount | Financial Controls aging in customer currency. Unpaid transactions billed minus unapplied payments deposited between 15 and 28 days.  Column Name: FC AR Aging 15-28 (Customer Currency)
fc_aging_15_28.secondary_amount | Financial Controls aging in client currency. Unpaid transactions billed minus unapplied payments deposited between 15 and 28 days.  Column Name: FC AR Aging 15-28 (Client Currency)
fc_aging_15_28.system_amount | Financial Controls aging in system currency. Unpaid transactions billed minus unapplied payments deposited between 15 and 28 days.  Column Name: FC AR Aging 15-28 (System Currency)
fc_aging_29_42.buyer_amount | Financial Controls aging in customer currency. Unpaid transactions billed minus unapplied payments deposited between 29 and 42 days.  Column Name: FC AR Aging 29-42 (Customer Currency)
fc_aging_29_42.secondary_amount | Financial Controls aging in client currency. Unpaid transactions billed minus unapplied payments deposited between 29 and 42 days.  Column Name: FC AR Aging 29-42 (Client Currency)
fc_aging_29_42.system_amount | Financial Controls aging in system currency. Unpaid transactions billed minus unapplied payments deposited between 29 and 42 days.  Column Name: FC AR Aging 29-42 (System Currency)
fc_aging_43_56.buyer_amount | Financial Controls aging in customer currency. Unpaid transactions billed minus unapplied payments deposited between 43 and 56 days.  Column Name: FC AR Aging 43-56 (Customer Currency)
fc_aging_43_56.seconday_amount | Financial Controls aging in client currency. Unpaid transactions billed minus unapplied payments deposited between 43 and 56 days.  Column Name: FC AR Aging 43-56 (Client Currency)
fc_aging_43_56.system_amount | Financial Controls aging in system currency. Unpaid transactions billed minus unapplied payments deposited between 43 and 56 days.  Column Name: FC AR Aging 43-56 (System Currency)
fc_aging_57_91.buyer_amount | Financial Controls aging in customer currency. Unpaid transactions billed minus unapplied payments deposited between 57 and 91 days.  Column Name: FC AR Aging 57-91 (Customer Currency)
fc_aging_57_91.secondary_amount | Financial Controls aging in client currency. Unpaid transactions billed minus unapplied payments deposited between 57 and 91 days.  Column Name: FC AR Aging 57-91 (Client Currency)
fc_aging_57_91.system_amount | Financial Controls aging in system currency. Unpaid transactions billed minus unapplied payments deposited between 57 and 91 days.  Column Name: FC AR Aging 57-91 (System Currency)
fc_aging_over_91.buyer_amount | Financial Controls aging in customer currency. Unpaid transactions billed minus unapplied payments deposited over 91 days.  Column Name: FC AR Aging 92+(Customer Currency)
fc_aging_over_91.secondary_amount | Financial Controls aging in client currency. Unpaid transactions billed minus unapplied payments deposited over 91 days.  Column Name: FC AR Aging 92+(Client Currency)
fc_aging_over_91.system_amount | Financial Controls aging in system currency. Unpaid transactions billed minus unapplied payments deposited over 91 days.  Column Name: FC AR Aging 92+(System Currency)
last_payment.buyer_amount | Last payment amount posted to billing customers account in billing customers currency. Column Name: Last Payment Amount (Customer Currency)
last_payment.secondary_amount | Last payment amount posted to billing customers account in client currency. Column Name: Last Payment Amount (Client Currency)
last_payment.system_amount | Last payment amount posted to billing customers account in system currency. Column Name: Last Payment Amount (System Currency)
lytd_days_to_pay | Last year to date days to pay. This is calculated by the average number of days between the billing date and payment date for all payments deposited from first day of last year to todays date last year.  Column Name: Days to Pay LYTD
lytd_purchases.buyer_amount | Total purchases since the first of last year and todays date last year in customer currency. This is calculated by using the billing date and summing billed activity.  Column Name: Purchases LYTD (Customer  Currency)
lytd_purchases.system_amount | Total purchases since the first of last year and todays date last year in system currency. This is calculated by using the billing date and summing billed activity.  Column Name: Purchases LYTD (System  Currency)
lytot_purchases.buyer_amount | Total purchases last year in customer currency. This is calculated by using the billing date and summing billed activity.  Column Name: Purchases LYTOT (Customer Currency)
lytot_purchases.system_amount | Total purchases last year in system currency. This is calculated by using the billing date and summing billed activity.  Column Name: Purchases LYTOT (System Currency)
network_name | Billing customers network. Column Name: Network
open_disputed_total.buyer_amount | Total unpaid transactions that are marked in dispute in customer currency. Column Name: Unpaid Disputed Total (Customer Currency)
open_disputed_total.secondary_amount | Total unpaid transactions that are marked in dispute in customer currency. Column Name: Unpaid Disputed Total (Client Currency)
open_disputed_total.system_amount | Total unpaid transactions that are marked in dispute in customer currency. Column Name: Unpaid Disputed Total (System Currency)
open_late_fee_total.buyer_amount | Total unpaid late charges in customer currency. Column Name: Open Late Charge Total (Customer Currency)
open_late_fee_total.secondary_amount | Total unpaid late charges in client currency. Column Name: Open Late Charge Total (Client Currency)
open_late_fee_total.system_amount | Total unpaid late charges in system currency. Column Name: Open Late Charge Total (System Currency)
open_trx_total.buyer_amount | Total unpaid transactions in customer currency. Column Name: Open Transaction Total (Customer Currency)
open_trx_total.secondary_amount | Total unpaid transactions in client currency. Column Name: Open Transaction Total (Client Currency)
open_trx_total.system_amount | Total unpaid transactions in system currency. Column Name: Open Transaction Total (System Currency)
pending_billing.buyer_amount | Total transactions pending to be billed in customer currency.  Column Name: Pending to Bill (Customer Currency)
pending_billing.secondary_amount | Total transactions pending to be billed in client currency.  Column Name: Pending to Bill (Client Currency)
pending_billing.system_amount | Total transactions pending to be billed in system currency. Column Name: Pending to Bill (System Currency)
program\_id | The program under the client Apruve, e.g., Apruve.
purchase_buyer_id | Purchasing customer ID. This is the typically the card holder and can be different than who gets billed for the purchase.  Column Name: Purchasing Customer ID
unapplied_payment_total.buyer_amount | Total unapplied payments in customer currency. Column Name: Unapplied Payment Total (Customer Currency)
unapplied_payment_total.secondary_amount | Total unapplied payments in client currency. Column Name: Unapplied Payment Total (Client Currency)
unapplied_payment_total.system_amount | Total unapplied payments in system currency. Column Name: Unapplied Payment Total (System Currency)
ytd_days_to_pay | This year to date days to pay. This is calculated by the average number of days between the billing date and payment date for all payments deposited from first day of this year to todays date. Column Name: Days To Pay YTD
ytd_purchases.buyer_amount | Total purchases since the first of the year in customer currency. This is calculated by using the billing date and summing billed activity.  Column Name: Purchases YTD (Customer Currency)
ytd_purchases.system_amount | Total purchases since the first of the year in system currency. This is calculated by using the billing date and summing billed activity.  Column Name: Purchases YTD (System Currency)

## Notes
