{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "https://enterprise-messaging.msts.com/schemas/accounts_receivable/v1.2/schema.json",
    "definitions": {
      "originSystemIdentifier": {
        "$id": "#/definitions/originSystemIdentifier",
        "type": "string",
        "title": "The Origin System Identifier Schema",
        "description": "An ID generated in a MSTS owned system that is the origin of the relevant data",
        "examples": [
          "2dec5b9d-f37f-4733-964e-8e5378c896fb"
        ],
        "maxLength": 50
      },
      "currencyCode": {
        "$id": "#/definitions/currencyCode",
        "type": "string",
        "title": "The Currency Code Schema",
        "description": "The ISO 4217 code for a currency",
        "examples": [
          "USD",
          "CAD"
        ],
        "anyOf": [{
            "$comment": "Currency codes originally pulled from SPHERE.fw_r_currency_ri",
            "enum": [
              "ADP", "AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "ATS", "AUD", "AWG", "AZM",
              "BAM", "BBD", "BDT", "BEF", "BGL", "BGN", "BHD", "BIF", "BMD", "BND", "BOB", "BOV", "BRL", "BSD", "BTN", "BWP", "BYR", "BZD",
              "CAD", "CDF", "CHF", "CLF", "CLP", "CNY", "COP", "COU", "CRC", "CSD", "CUP", "CVE", "CYP", "CZK",
              "DEM", "DJF", "DKK", "DOP", "DZD",
              "ECS", "ECV", "EEK", "EGP", "ERN", "ESP", "ETB", "EUR",
              "FIM", "FJD", "FKP", "FRF",
              "GBP", "GEL", "GHC", "GIP", "GMD", "GNF", "GRD", "GTQ", "GWP", "GYD",
              "HKD", "HNL", "HRK", "HTG", "HUF",
              "IDR", "IEP", "ILS", "INR", "IQD", "IRR", "ISK", "ITL", "JMD", "JOD", "JPY",
              "KES", "KGS", "KHR", "KMF", "KPW", "KRW", "KWD", "KYD", "KZT",
              "LAK", "LBP", "LKR", "LRD", "LSL", "LTL", "LUF", "LVL", "LYD",
              "MAD", "MDL", "MGA", "MGF", "MKD", "MMK", "MNT", "MOP", "MRO", "MTL", "MUR", "MVR", "MWK", "MXN", "MXV", "MYR", "MZM", "MZN",
              "NAD", "NGN", "NIO", "NLG", "NOK", "NPR", "NZD",
              "OMR",
              "PAB", "PEN", "PGK", "PHP", "PKR", "PLN", "PTE", "PYG",
              "QAR",
              "ROL", "RON", "RSD", "RUB", "RWF",
              "SAR", "SBD", "SCR", "SDD", "SDG", "SEK", "SGD", "SHP", "SIT", "SKK", "SLL", "SOS", "SRD", "SRG", "STD", "SVC", "SYP", "SZL",
              "THB", "TJS", "TMM", "TND", "TOP", "TRL", "TRY", "TTD", "TWD", "TZS",
              "UAH", "UGX", "USD", "UYU", "UZS",
              "VEB", "VND", "VUV",
              "WST",
              "XAF", "XCD", "XOF", "XPF",
              "YER", "YUM",
              "ZAR", "ZMK", "ZWD"
            ]
          },
          {
            "$comment": "ISO 4217 allow super-national and private currencies that start with X followed by two letters",
            "pattern": "^X[A-Z]{2}$"
          }
        ]
      },
      "basicCurrencyAmountGroup": {
        "$id": "#/definitions/basicCurrencyAmountGroup",
        "type": "object",
        "title": "The Basic Currency Amount Group Schema",
        "properties": {
          "secondary_amount": {
            "$id": "#/definitions/basicCurrencyAmountGroup/properties/secondary_amount",
            "type": "number",
            "title": "The Secondary_amount Schema",
            "description": "The relevant amount in the system's secondary currency",
            "examples": [
              123.35
            ]
          },
          "buyer_amount": {
            "$id": "#/definitions/basicCurrencyAmountGroup/properties/buyer_amount",
            "type": "number",
            "title": "The Buyer_amount Schema",
            "description": "The relevant amount in the buyer's currency",
            "examples": [
              123.35
            ]
          },
          "seller_amount": {
            "$id": "#/definitions/basicCurrencyAmountGroup/properties/seller_amount",
            "type": "number",
            "title": "The seller_amount Schema",
            "description": "The relevant amount in the seller's currency",
            "examples": [
              123.35
            ]
          },
          "system_amount": {
            "$id": "#/definitions/basicCurrencyAmountGroup/properties/system_amount",
            "type": "number",
            "title": "The System_amount Schema",
            "description": "The relevant amount in the system's currency",
            "examples": [
              1223.35
            ]
          }
        },
        "patternProperties": {
          "^[xX]-": true
        },
        "additionalProperties": false
      },
      "buyerCurrencyAmountGroup": {
        "$id": "#/definitions/buyerCurrencyAmountGroup",
        "type": "object",
        "title": "The Currency Amount Group Schema",
        "required": [
          "secondary_amount",
          "buyer_amount",
          "system_amount"
        ],
        "$comment": "Setting properties keys to false rejects these properties that are defined in the parent schema basicCurrencyAmountGroup",
        "properties": {
          "seller_amount": false
        },
        "allOf": [{
          "$ref": "#/definitions/basicCurrencyAmountGroup"
        }]
      },
      "sellerCurrencyAmountGroup": {
        "$id": "#/definitions/sellerCurrencyAmountGroup",
        "type": "object",
        "title": "The Currency Amount Group Schema",
        "required": [
          "secondary_amount",
          "seller_amount",
          "system_amount"
        ],
        "$comment": "Setting properties keys to false rejects these properties that are defined in the parent schema basicCurrencyAmountGroup",
        "properties": {
          "buyer_amount": false
        },
        "allOf": [{
          "$ref": "#/definitions/basicCurrencyAmountGroup"
        }]
      },
      "systemCurrencyAmountGroup": {
        "$id": "#/definitions/systemCurrencyAmountGroup",
        "type": "object",
        "title": "The Currency Amount Group Schema",
        "required": [
          "secondary_amount",
          "system_amount"
        ],
        "$comment": "Setting properties keys to false rejects these properties that are defined in the parent schema basicCurrencyAmountGroup",
        "properties": {
          "buyer_amount": false,
          "seller_amount": false
        },
        "allOf": [{
          "$ref": "#/definitions/basicCurrencyAmountGroup"
        }]
      },
      "fullCurrencyAmountGroup": {
        "$id": "#/definitions/fullCurrencyAmountGroup",
        "type": "object",
        "title": "The Currency Amount Group Schema",
        "required": [
          "secondary_amount",
          "buyer_amount",
          "seller_amount",
          "system_amount"
        ],
        "allOf": [{
          "$ref": "#/definitions/basicCurrencyAmountGroup"
        }]
      }
    },
    "type": "object",
    "title": "MSTS AR Schema",
    "description": "This object details Accounts Recievable data for a given billing buyer.",
    "required": [
      "created_at",
      "type",
      "version",
      "id",
      "origin",
      "payload"
    ],
    "properties": {
      "created_at": {
        "$id": "#/properties/created_at",
        "type": "string",
        "title": "The Created_at Schema",
        "description": "When this message was created.",
        "default": "",
        "examples": [
          "2018-10-11T03:34:57.039854+00:00"
        ],
        "format": "date-time"
      },
      "type": {
        "$id": "#/properties/type",
        "type": "string",
        "title": "The Type Schema",
        "description": "The standard entity name for this format.",
        "default": "accounts_receivable",
        "examples": [
          "accounts_receivable"
        ],
        "pattern": "^(.*)$"
      },
      "version": {
        "$id": "#/properties/version",
        "type": "string",
        "title": "The Version Schema",
        "description": "The version of this message format.",
        "default": "1.2",
        "examples": [
          "1.2"
        ]
      },
      "id": {
        "$id": "#/properties/id",
        "title": "The Id Schema",
        "description": "The Unique ID of this message from teh producer's perspective.",
        "default": "",
        "examples": [
          "4543c4-eac4-41ac-9403-df083c2b4482"
        ],
        "allOf": [{
          "$ref": "#/definitions/originSystemIdentifier"
        }]  
      },
      "origin": {
        "$id": "#/properties/origin",
        "type": "string",
        "title": "The Origin Schema",
        "description": "The producer of this message.",
        "default": "",
        "examples": [
          "BESTBUY"
        ],
        "pattern": "^(.*)$"
      },
      "payload": {
        "$id": "#/properties/payload",
        "type": "object",
        "title": "The Payload Schema",
        "description": "The payload of the message.",
        "required": [
          "client_id",
          "client_id_legacy",
          "billing_buyer_id",
          "program_id",
          "purchase_buyer_id"
        ],
        "properties": {
          "ar_total": {
            "$id": "#/properties/payload/properties/ar_total",
            "type": "object",
            "title": "The Ar_total Schema",
            "description": "This is the total unpaid transactions minus unapplied payments.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "auth_total": {
            "$id": "#/properties/payload/properties/auth_total",
            "type": "object",
            "title": "The Auth_total Schema",
            "description": "Authorizations that have not expired and have not been matched to a processed transactions.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "bill_ar_owner": {
            "$id": "#/properties/payload/properties/bill_ar_owner",
            "type": "string",
            "title": "The Bill_ar_owner Schema",
            "description": "Accounts Receivable Owner - Who's bank account are payments going through for this transaction and who is responsible for exposure if not in recourse.",
            "default": "",
            "examples": [
              "10149"
            ],
            "pattern": "^(.*)$"
          },
          "billing_buyer_id": {
            "$id": "#/properties/payload/properties/billing_buyer_id",
            "title": "The Billing_buyer_id Schema",
            "description": "The billing Buyer ID.",
            "default": "",
            "examples": [
              "10149"
            ],
            "allOf": [{
              "$ref": "#/definitions/originSystemIdentifier"
            }]
          },
          "billing_buyer": {
            "$id": "#/properties/payload/properties/billing_buyer",
            "type": "object",
            "title": "The Billing_buyer Schema",
            "description": "Information about the billing Buyer.",
            "required": [
              "amount_credit",
              "amount_exposure",
              "available_credit"
            ],
            "properties": {
              "amount_credit": {
                "$id": "#/properties/payload/properties/billing_buyer/properties/amount_credit",
                "type": "number",
                "title": "The Amount_credit Schema",
                "description": "Credit line assigned to the billing customer. If they are drawing their credit of their parent then this will define the billing customers limit of the parents credit line.",
                "default": 0,
                "examples": [
                  75000
                ]
              },
              "amount_exposure": {
                "$id": "#/properties/payload/properties/billing_buyer/properties/amount_exposure",
                "type": "number",
                "title": "The Amount_exposure Schema",
                "description": "The amount that has been used of their credit line. This is made up of active authorizations + transactions pending to be billed + unpaid (open) transactions.",
                "default": 0.0,
                "examples": [
                  -1599.6
                ]
              },
              "available_credit": {
                "$id": "#/properties/payload/properties/billing_buyer/properties/available_credit",
                "type": "number",
                "title": "The Available_credit Schema",
                "description": "This is the billing buyer's credit line minus their exposure.",
                "default": 0.0,
                "examples": [
                  76599.6
                ]
              }
            }
          },
          "client_id": {
            "$id": "#/properties/payload/properties/client_id",
            "title": "The Client_id Schema",
            "description": "For GUESS projects this is analogous to the Network. So for 'PACCAR - Mexico' they will submit 'PM'. The network/client Cummins would submit 'CU'. For Australia projects, this will be the ID for the client, e.g., 74243728-8819-4cc0-9585-774e2ab1e7fc as the ID for Apruve. This value cannot change and must be unique.",
            "default": "",
            "examples": [
              ""
            ],
            "allOf": [{
              "$ref": "#/definitions/originSystemIdentifier"
            }]
          },
          "client_id_legacy": {
            "$id": "#/properties/payload/properties/client_id_legacy",
            "title": "The Client_id_legacy Schema",
            "description": "For ALL projects this will be the 2-letter Network Identifier. So 'PACCAR - Australia' would submit 'PA', PACCAR would submit 'PP', Cummins would submit 'CU'.",
            "default": "",
            "examples": [
              "0"
            ],
            "allOf": [{
              "$ref": "#/definitions/originSystemIdentifier"
            }]
          },
          "client_identifier": {
            "$id": "#/properties/payload/properties/client_identifier",
            "title": "The Client_identifier Schema",
            "description": "The clients identifier for the buyer. If blank then the project either does not have a client or the client did not provide their identifier for this buyer.",
            "default": "",
            "examples": [
              ""
            ],
            "allOf": [{
              "$ref": "#/definitions/originSystemIdentifier"
            }]
          },
          "credit_aging_1_7": {
            "$id": "#/properties/payload/properties/credit_aging_1_7",
            "type": "object",
            "title": "The Credit_aging_1_7 Schema",
            "description": "Total unpaid amount past due between 1 and 7 days. Unapplied credits are not aged. ",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "credit_aging_15_30": {
            "$id": "#/properties/payload/properties/credit_aging_15_30",
            "type": "object",
            "title": "The Credit_aging_15_30 Schema",
            "description": "Total unpaid amount past due between 15 and 30 days. Unapplied credits are not aged. ",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "credit_aging_31_45": {
            "$id": "#/properties/payload/properties/credit_aging_31_45",
            "type": "object",
            "title": "The Credit_aging_31_45 Schema",
            "description": "Total unpaid amount past due between 31 and 45 days. Unapplied credits are not aged. ",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "credit_aging_46_60": {
            "$id": "#/properties/payload/properties/credit_aging_46_60",
            "type": "object",
            "title": "The Credit_aging_46_60 Schema",
            "description": "Total unpaid amount past due between 45 and 60 days. Unapplied credits are not aged. ",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "credit_aging_61_90": {
            "$id": "#/properties/payload/properties/credit_aging_61_90",
            "type": "object",
            "title": "The Credit_aging_61_90 Schema",
            "description": "Total unpaid amount past due between 61 and 90 days. Unapplied credits are not aged. ",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "credit_aging_8_14": {
            "$id": "#/properties/payload/properties/credit_aging_8_14",
            "type": "object",
            "title": "The Credit_aging_8_14 Schema",
            "description": "Total unpaid amount past due between 8 and 14 days. Unapplied credits are not aged. ",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "credit_aging_91_120": {
            "$id": "#/properties/payload/properties/credit_aging_91_120",
            "type": "object",
            "title": "The Credit_aging_91_120 Schema",
            "description": "Total unpaid amount past due between 91 and 120 days. Unapplied credits are not aged. ",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "credit_aging_late_fee_total": {
            "$id": "#/properties/payload/properties/credit_aging_late_fee_total",
            "type": "object",
            "title": "The Credit_aging_late_fee_total Schema",
            "description": "Total of late fees that are past due.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "credit_aging_less_pay_total": {
            "$id": "#/properties/payload/properties/credit_aging_less_pay_total",
            "type": "object",
            "title": "The Credit_aging_less_pay_total Schema",
            "description": "Total of transactions past due less unapplied payments.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "credit_aging_over_120": {
            "$id": "#/properties/payload/properties/credit_aging_over_120",
            "type": "object",
            "title": "The Credit_aging_over_120 Schema",
            "description": "Total unpaid amount past due over 120 days. Unapplied credits are not aged.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "credit_aging_over_60": {
            "$id": "#/properties/payload/properties/credit_aging_over_60",
            "type": "object",
            "title": "The Credit_aging_over_60 Schema",
            "description": "Total unpaid amount past due over 60 days. Unapplied credits are not aged.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "credit_buyer_id": {
            "$id": "#/properties/payload/properties/credit_buyer_id",
            "title": "The Credit_buyer_id Schema",
            "description": "The credit Buyer ID.",
            "default": "",
            "examples": [
              "10149"
            ],
            "allOf": [{
              "$ref": "#/definitions/originSystemIdentifier"
            }]
          },
          "credit_buyer": {
            "$id": "#/properties/payload/properties/credit_buyer",
            "type": "object",
            "title": "The Credit_buyer Schema",
            "description": "Information about the credit Buyer.",
            "required": [
              "amount_credit",
              "amount_exposure"
            ],
            "properties": {
              "amount_credit": {
                "$id": "#/properties/payload/properties/credit_buyer/properties/amount_credit",
                "type": "number",
                "title": "The Amount_credit Schema",
                "description": "Credit line assigned to the credit buyer for the billing buyer. If they are using their own credit then this will be the same as the billing credit line.",
                "default": 0,
                "examples": [
                  75000
                ]
              },
              "amount_exposure": {
                "$id": "#/properties/payload/properties/credit_buyer/properties/amount_exposure",
                "type": "number",
                "title": "The Amount_exposure Schema",
                "description": "The amount that has been used of the credit buyer's credit line. This is made up of active authorizations + transactions pending to be billed + unpaid (open) transactions for all buyer's drawing off of their credit.",
                "default": 0.0,
                "examples": [
                  -1599.6
                ]
              }
            }
          },
          "credit_extended_ar_total": {
            "$id": "#/properties/payload/properties/credit_extended_ar_total",
            "type": "object",
            "title": "The Credit_extended_ar_total Schema",
            "description": "Total transactions not past due.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "currencies": {
            "$id": "#/properties/payload/properties/currencies",
            "type": "object",
            "title": "The Currencies Schema",
            "description": "Listings of application currencies.",
            "required": [
              "buyer",
              "secondary",
              "system"
            ],
            "properties": {
              "buyer": {
                "$id": "#/properties/payload/properties/currencies/properties/buyer",
                "type": "string",
                "title": "The Buyer Schema",
                "description": "The buyer's currency.",
                "default": "",
                "examples": [
                  "USD"
                ],
                "allOf": [{
                  "$ref": "#/definitions/currencyCode"
                }]
              },
              "secondary": {
                "$id": "#/properties/payload/properties/currencies/properties/secondary",
                "type": "string",
                "title": "The Secondary Schema",
                "description": "Currency that the client totals are in.",
                "default": "",
                "examples": [
                  "USD"
                ],
                "allOf": [{
                  "$ref": "#/definitions/currencyCode"
                }]
              },
              "system": {
                "$id": "#/properties/payload/properties/currencies/properties/system",
                "type": "string",
                "title": "The System Schema",
                "description": "The system's currency.",
                "default": "",
                "examples": [
                  "USD"
                ],
                "allOf": [{
                  "$ref": "#/definitions/currencyCode"
                }]
              }
            }
          },
          "current_days_to_pay": {
            "$id": "#/properties/payload/properties/current_days_to_pay",
            "type": "number",
            "title": "The Current_days_to_pay Schema",
            "description": "Billing buyers current days to pay. This is calculated by taking the last four applied payments and averaging the number of days between the billing date and payment date.",
            "default": 0.0,
            "examples": [
              27.67
            ]
          },
          "disputed_total": {
            "$id": "#/properties/payload/properties/disputed_total",
            "type": "object",
            "title": "The Disputed_total Schema",
            "description": "Total transactions that are marked in dispute.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "fc_aging_0_14": {
            "$id": "#/properties/payload/properties/fc_aging_0_14",
            "type": "object",
            "title": "The Fc_aging_0_14 Schema",
            "description": "Financial Controls aging. Unpaid transactions billed minus unapplied payments deposited between 0 and 14 days.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "fc_aging_15_28": {
            "$id": "#/properties/payload/properties/fc_aging_15_28",
            "type": "object",
            "title": "The Fc_aging_15_28 Schema",
            "description": "Financial Controls aging. Unpaid transactions billed minus unapplied payments deposited between 15 and 28 days.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "fc_aging_29_42": {
            "$id": "#/properties/payload/properties/fc_aging_29_42",
            "type": "object",
            "title": "The Fc_aging_29_42 Schema",
            "description": "Financial Controls aging. Unpaid transactions billed minus unapplied payments deposited between 29 and 42 days.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "fc_aging_43_56": {
            "$id": "#/properties/payload/properties/fc_aging_43_56",
            "type": "object",
            "title": "The Fc_aging_43_56 Schema",
            "description": "Financial Controls aging. Unpaid transactions billed minus unapplied payments deposited between 43 and 56 days.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "fc_aging_57_91": {
            "$id": "#/properties/payload/properties/fc_aging_57_91",
            "type": "object",
            "title": "The Fc_aging_57_91 Schema",
            "description": "Financial Controls aging. Unpaid transactions billed minus unapplied payments deposited between 57 and 91 days.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "fc_aging_over_91": {
            "$id": "#/properties/payload/properties/fc_aging_over_91",
            "type": "object",
            "title": "The Fc_aging_over_91 Schema",
            "description": "Financial Controls aging. Unpaid transactions billed minus unapplied payments deposited over 90 days.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "last_payment": {
            "$id": "#/properties/payload/properties/last_payment",
            "type": "object",
            "title": "The Last_payment Schema",
            "description": "Last payment amount posted to billing buyer's account.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "lytd_days_to_pay": {
            "$id": "#/properties/payload/properties/lytd_days_to_pay",
            "type": "number",
            "title": "The Lytd_days_to_pay Schema",
            "description": "Last year to date days to pay. This is calculated by the average number of days between the billing date and payment date for all payments deposited from first day of last year to todays date last year.",
            "default": 0.0,
            "examples": [
              27.67
            ]
          },
          "lytd_purchases": {
            "$id": "#/properties/payload/properties/lytd_purchases",
            "type": "object",
            "title": "The Lytd_purchases Schema",
            "description": "Total purchases since the first of last year and todays date last year. This is calculated by using the billing date and summing billed activity. ",
            "required": [
              "buyer_amount",
              "system_amount"
            ],
            "allOf": [{
              "$ref": "#/definitions/basicCurrencyAmountGroup"
            }]  
          },
          "lytot_purchases": {
            "$id": "#/properties/payload/properties/lytot_purchases",
            "type": "object",
            "title": "The Lytot_purchases Schema",
            "description": "Total purchases last year. This is calculated by using the billing date and summing billed activity.",
            "required": [
              "buyer_amount",
              "system_amount"
            ],
            "allOf": [{
              "$ref": "#/definitions/basicCurrencyAmountGroup"
            }]  
          },
          "network_name": {
            "$id": "#/properties/payload/properties/network_name",
            "type": "string",
            "title": "The Network_name Schema",
            "description": "Billing buyer's network.",
            "default": "",
            "examples": [
              "Best Buy"
            ],
            "pattern": "^(.*)$"
          },
          "open_disputed_total": {
            "$id": "#/properties/payload/properties/open_disputed_total",
            "type": "object",
            "title": "The Open_disputed_total Schema",
            "description": "Total unpaid transactions that are marked in dispute.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "open_late_fee_total": {
            "$id": "#/properties/payload/properties/open_late_fee_total",
            "type": "object",
            "title": "The Open_late_fee_total Schema",
            "description": "Total unpaid late charges.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "open_trx_total": {
            "$id": "#/properties/payload/properties/open_trx_total",
            "type": "object",
            "title": "The Open_trx_total Schema",
            "description": "Total unpaid transactions.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "pending_billing": {
            "$id": "#/properties/payload/properties/pending_billing",
            "type": "object",
            "title": "The Pending_billing Schema",
            "description": "Total transactions pending to be billed.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "program_id": {
            "$id": "#/properties/payload/properties/program_id",
            "title": "The Program_id Schema",
            "description": "The program under the client Apruve, e.g., Apruve.",
            "allOf": [{
              "$ref": "#/definitions/originSystemIdentifier"
            }]
          },
          "purchase_buyer_id": {
            "$id": "#/properties/payload/properties/purchase_buyer_id",
            "type": "string",
            "title": "The Purchase_buyer_id Schema",
            "description": "Purchasing buyer ID. This is the typically the card holder and can be different than who gets billed for the purchase.",
            "default": "",
            "examples": [
              "10149"
            ],
            "pattern": "^(.*)$"
          },
          "unapplied_payment_total": {
            "$id": "#/properties/payload/properties/unapplied_payment_total",
            "type": "object",
            "title": "The Unapplied_payment_total Schema",
            "description": "Total unapplied payments.",
            "allOf": [{
              "$ref": "#/definitions/buyerCurrencyAmountGroup"
            }]  
          },
          "ytd_days_to_pay": {
            "$id": "#/properties/payload/properties/ytd_days_to_pay",
            "type": "number",
            "title": "The Ytd_days_to_pay Schema",
            "description": "This year to date days to pay. This is calculated by the average number of days between the billing date and payment date for all payments deposited from first day of this year to todays date.",
            "default": 0,
            "examples": [
              0
            ]
          },
          "ytd_purchases": {
            "$id": "#/properties/payload/properties/ytd_purchases",
            "type": "object",
            "title": "The Ytd_purchases Schema",
            "description": "Total purchases since the first of the year. This is calculated by using the billing date and summing billed activity.",
            "required": [
              "buyer_amount",
              "system_amount"
            ],
            "allOf": [{
              "$ref": "#/definitions/basicCurrencyAmountGroup"
            }]  
          }
        }
      }
    }
  }
